using Testura.Code;

using Xunit;

namespace Code.Test {

    public class ClassGenerationTest {

        [Fact]
        public void Class_must_be_generated( ) {
            var @class = new Class( "Startup" )
                .WithNamespace( "Harpy.Template" )
                .WithUsings( "Hangfire", "Harpy.IoC" )
                .WithUsing( "Microsoft" )
                .WithField( "_defaultConnection", typeof( string ), Modifiers.Private, Modifiers.Readonly )
                .WithField( "_hangfireConnection", typeof( string ), Modifiers.Private, Modifiers.Readonly )
                .WithField( "_configuration", "IConfiguration", Modifiers.Private, Modifiers.Readonly )
                .WithModifiers( Modifiers.Public )
                .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameter( "configuration", "IConfiguration" )
                    .WithLogic( variable => variable
                        .WithVariable( "_configuration", variable => variable.AssignAs( "configuration" ) )
                        .WithVariable( "_defaultConnetion", variable => variable.AssignAs( "configuration", "GetConnectionStringFile", Argument.Value( "DefaultConnection" ) ) )
                        .WithVariable( "_hangfireConnection", variable => variable.AssignAs( "configuration", "GetConnectionStringFile", Argument.Value( "HangfireConnection" ) ) ) ) )
                .WithMethod( "ConfigureService", method => method
                     .WithModifiers( Modifiers.Public )
                     .WithParameter( "services", "IServiceCollection" )
                     .WithLogic( logic => logic
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddSwaggerMiddleware", Argument.Member( "AppVersion", "GetCurrent" ) ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddDefaultCorsPolice" ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddCulture", Argument.Value( "\"en-US\"" ) ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddDatabase",
                            Argument.Lambda( "connection",
                                Reference.Method( "UseSqliteConnection",
                                    Argument.Variable( "_defaultConnection" ),
                                    Argument.Lambda( "options", Reference.Method( "AddMigrations", Argument.Value( "Spartan.Migrations" ) ) ) ) ),
                            Argument.Lambda( "context", Reference.Method( "AddContext<ExempleContext>" ) ),
                            Argument.Lambda( "options", Reference.Chained(
                                    Reference.Method( "EnableLog" ),
                                    Reference.Method( "EnableMigrations",
                                        Argument.Lambda( "migrationOptions", Reference.Method( "EnsureAllMigrationsApplied" ) ) ) ) ) ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddHarpy",
                             Argument.Lambda( "settings", Reference.Member( "JobStorage", Reference.Chained(
                                Reference.Variable( "HarpySQLite" ),
                                Reference.Method( "UseStorage",
                                    Argument.Variable( "_hangfireConnection" ) ) ) ) ) ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddEndpoints" ) ) ) )
                .WithMethod( "Configure", method => method
                     .WithModifiers( Modifiers.Public )
                     .WithParameter( "app", "IApplicationBuilder" )
                     .WithLogic( logic => logic
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseStaticFiles", Argument.Lambda( "file", Reference.Chained( Reference.Method( "IncludePath", Argument.Member( "Directory", "GetCurrentDirectory", Argument.Value( "images" ) ) ) ) ) ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseCors", Argument.Value( "DefaultPolicy" ) ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseHarpySwagger" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseHangdireDashboard" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseExceptionMiddleware" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseAuthorizationMiddleware" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseRouting" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseAuthentication" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseAuthorization" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseEndpoints", Argument.Lambda( "endpoints", Reference.Method( "MapControllers" ) ) ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseHttpsRedirection" ) )

                ) );

            var test = @class.ToString( );

            Assert.NotNull( test );
        }
    }
}