﻿using Microsoft.CodeAnalysis.CSharp.Formatting;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Testura.Code;
using Testura.Code.Builders;
using Testura.Code.Models;
using Testura.Code.Models.Options;
using Testura.Code.Models.Properties;
using Testura.Code.Models.Types;
using Testura.Code.Saver;
using Attribute = Testura.Code.Models.Attribute;

namespace Code {

    public class Class : Builder {
        public string Name { get; private set; }

        private string Namespace { get; set; }

        private List<string> Usings { get; set; }

        private List<Field> Fields { get; set; }

        private List<Modifiers> ModifierList { get; set; }

        private List<Constructor> Constructors { get; set; }

        private List<Method> Methods { get; set; }

        private List<Property> Properties { get; set; }

        private List<Type> Inheritance { get; set; }

        private List<Attribute> Attributes { get; set; }

        public Class( string name ) {
            Name = name;
            Usings = new List<string>( );
            Fields = new List<Field>( );
            ModifierList = new List<Modifiers>( );
            Constructors = new List<Constructor>( );
            Methods = new List<Method>( );
            Properties = new List<Property>( );
            Inheritance = new List<Type>( );
            Attributes = new List<Attribute>( );
        }

        public Class WithNamespace( params string[ ] namespacePieces ) {
            Namespace = string.Join( ".", namespacePieces.ToArray( ) ).Replace( '/', '.' );
            return this;
        }

        public Class WithNamespace( string @namespace ) {
            Namespace = @namespace;
            return this;
        }

        public Class WithUsing( string @using ) {
            Usings.Add( @using );
            return this;
        }

        public Class WithUsings( params string[ ] usings ) {
            Usings.AddRange( usings );
            return this;
        }

        public Class WithField( string field, Type type, params Modifiers[ ] modifiers ) {
            Fields.Add( new Field( field, type, modifiers.ToList( ) ) );
            return this;
        }

        public Class WithField( string field, string type, params Modifiers[ ] modifiers ) {
            Fields.Add( new Field( field, CustomType.Create( type ), modifiers.ToList( ) ) );
            return this;
        }

        public Class WithFields( params Field[ ] fields ) {
            Fields.AddRange( fields );
            return this;
        }

        public Class WithModifiers( params Modifiers[ ] modifiers ) {
            ModifierList.AddRange( modifiers );
            return this;
        }

        public Class WithAttribute( string name, params Argument[ ] arguments ) {
            Attributes.Add( new Attribute( name, arguments.Build( ).ToList( ) ) );
            return this;
        }

        public Class WithAttributes( params Attribute[ ] attributes ) {
            Attributes.AddRange( attributes );
            return this;
        }

        public Class WithConstructor( Action<Constructor> action ) {
            var constructor = new Constructor( Name );
            action.Invoke( constructor );

            Constructors.Add( constructor );

            return this;
        }

        public Class WithMethod( string name, Action<Method> action ) {
            var method = new Method( name );
            action.Invoke( method );

            Methods.Add( method );

            return this;
        }

        public Class WithMethods( params Method[ ] methods ) {
            Methods.AddRange( methods );

            return this;
        }

        public Class WithProperty( string name, string type ) {
            WithProperty( name, CustomType.Create( type ) );
            return this;
        }

        public Class WithProperty( string name, Type type ) {
            var property = new AutoProperty(
                 name: name,
                 type: type,
                 propertyType: PropertyTypes.GetAndSet,
                 modifiers: new List<Modifiers> { Modifiers.Public },
                 setModifiers: new List<Modifiers> { Modifiers.Private } );

            Properties.Add( property );

            return this;
        }

        public Class WithProperties( params Property[ ] properties ) {
            Properties.AddRange( properties );
            return this;
        }

        public Class WithInheritance( Type type ) {
            Inheritance.Add( type );
            return this;
        }

        public Class WithInheritance( string type ) {
            WithInheritance( CustomType.Create( type ) );
            return this;
        }

        public CompilationUnitSyntax Build( ) {
            var builder = new ClassBuilder( Name, Namespace )
                .WithUsings( Usings.ToArray( ) )
                .WithFields( Fields.ToArray( ) )
                .WithModifiers( ModifierList.ToArray( ) )
                .WithProperties( Properties.OrderBy( x => x.SetModifiers == null ? 1 : 0 ).ToArray( ) )
                .WithConstructor( Constructors.Select( x => x.ToBuilder( ) ).ToArray( ) )
                .WithMethods( Methods.Select( x => x.ToBuilder( ) ).ToArray( ) )
                ;

            if ( Attributes.Any( ) )
                builder.WithAttributes( Attributes.ToArray( ) );

            if ( Inheritance.Any( ) )
                builder.ThatInheritFrom( Inheritance.ToArray( ) );

            return builder.Build( );
        }

        public override string ToString( ) =>
            new CodeSaver( new List<OptionKeyValue> {
                new OptionKeyValue( CSharpFormattingOptions.IndentBlock , true ),
            } )
                .SaveCodeAsString( Build( ) )
                .Replace( "\r\n        {\r\n        };", ";" );

        public override string ToFile( string path, bool forceReplace = false ) {
            var generatedCode = ToString( );
            if ( File.Exists( $"{path}.cs" ) && !forceReplace )
                throw new Exception( "File already exists. Set forceReplace as true to override the file." );
            else
                File.WriteAllText( $"{path}.cs", generatedCode );
            return generatedCode;
        }
    }
}