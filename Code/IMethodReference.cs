﻿using System.Collections.Generic;

namespace Code {

    public interface IMethodReference {
        List<Argument> Arguments { get; }

        string Name { get; }

        Method WithArguments( params Argument[ ] arguments );

        Method WithArguments( params string[ ] arguments );
    }
}