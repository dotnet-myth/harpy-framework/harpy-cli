﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;
using Testura.Code.Generators.Common;
using Testura.Code.Models.References;
using Testura.Code.Statements;

namespace Code {

    public class Reference {
        public string Name { get; set; }

        public Type ReferenceType { get; set; }

        public List<Argument> Arguments { get; set; }

        public List<Reference> References { get; set; }

        public Reference Assign { get; set; }

        public enum Type { Method, Member, Variable }

        public Reference( ) {
            Arguments = new List<Argument>( );
            References = new List<Reference>( );
        }

        public Reference( string name, Type referenceType ) : this( ) {
            Name = name;
            ReferenceType = referenceType;
        }

        public Reference( string name, Type referenceType, List<Argument> arguments ) {
            Name = name;
            ReferenceType = referenceType;
            Arguments = arguments;
            References = new List<Reference>( );
        }

        public Reference AssignTo( Reference reference ) {
            Assign = reference;
            return this;
        }

        public static Reference Method( string name, params Argument[ ] arguments ) {
            return new Reference( name, Type.Method, arguments.ToList( ) );
        }

        public static Reference Member( string name, Reference assignTo = null ) {
            return new Reference( name, Type.Member ).AssignTo( assignTo );
        }

        public static Reference Variable( string name ) {
            return new Reference( name, Type.Variable );
        }

        public static Reference Chained( params Reference[ ] references ) {
            var reference = references.First( );
            var list = references.ToList( );
            list.Remove( reference );
            reference.References.AddRange( list );
            return reference;
        }

        public ExpressionSyntax BuildExpression( ) {
            if ( Assign != null )
                return Statement.Declaration.Assign( Build( ), Assign.BuildExpression( ) ).Expression;
            else
                return ReferenceGenerator.Create( Build( ) );
        }

        public StatementSyntax BuildStatement( ) {
            return Statement.Declaration.Assign( Build( ), Assign.BuildExpression( ) );
        }

        public VariableReference Build( ) {
            VariableReference reference = BuildReference( );

            if ( References.Any( ) ) {
                var first = reference;
                var fila = new Queue<Reference>( References );
                while ( fila.TryDequeue( out var referenceItem ) ) {
                    first.Member = referenceItem.BuildReference( ) as MemberReference;
                    first = first.Member;
                }
            }

            return reference;
        }

        private VariableReference BuildReference( ) {
            switch ( ReferenceType ) {
                case Type.Method:
                    return new MethodReference( Name, Arguments.Build( ) );

                case Type.Member:
                    return new MemberReference( Name );

                case Type.Variable:
                default:
                    return new VariableReference( Name );
            }
        }
    }
}