﻿using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Testura.Code;
using Testura.Code.Builders;
using Testura.Code.Models.Types;
using Testura.Code.Saver;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace Code {

    public class Interface : Builder {
        private string Name { get; set; }

        private string Namespace { get; set; }

        private List<string> Usings { get; set; }

        private List<Type> Inheritance { get; set; }

        private List<Modifiers> ModifierList { get; set; }

        private List<Method> Methods { get; set; }

        public Interface( string name ) {
            Name = name;
            Usings = new List<string>( );
            ModifierList = new List<Modifiers>( );
            Inheritance = new List<Type>( );
            Methods = new List<Method>( );
        }

        public Interface WithNamespace( params string[ ] namespacePieces ) {
            Namespace = string.Join( ".", namespacePieces.ToArray( ) ).Replace( '/', '.' );
            return this;
        }

        public Interface WithNamespace( string @namespace ) {
            Namespace = @namespace;
            return this;
        }

        public Interface WithUsing( string @using ) {
            Usings.Add( @using );
            return this;
        }

        public Interface WithUsings( params string[ ] usings ) {
            Usings.AddRange( usings );
            return this;
        }

        public Interface WithModifiers( params Modifiers[ ] modifiers ) {
            ModifierList.AddRange( modifiers );
            return this;
        }

        public Interface WithInheritance( Type type ) {
            Inheritance.Add( type );
            return this;
        }

        public Interface WithInheritance( string type ) {
            WithInheritance( CustomType.Create( type ) );
            return this;
        }

        public Interface WithMethod( string name, Action<Method> action ) {
            var method = new Method( name );
            action.Invoke( method );

            Methods.Add( method );

            return this;
        }

        public CompilationUnitSyntax Build( ) {
            var builder = new InterfaceBuilder( Name, Namespace )
                .WithUsings( Usings.ToArray( ) )
                .ThatInheritFrom( Inheritance.ToArray( ) )
                .WithMethods( Methods.Select( x => x.ToBuilder( ).WithSemicolonToken( Token( SyntaxKind.SemicolonToken ) ) ).ToArray( ));

            return builder.Build( );
        }

        public override string ToString( ) =>
            new CodeSaver( )
                .SaveCodeAsString( Build( ) )
                .Replace( "\r\n        {\r\n        };", ";" );

        public override string ToFile( string path, bool forceReplace = false ) {
            var generatedCode = ToString( );
            if ( File.Exists( $"{path}.cs" ) && !forceReplace )
                throw new Exception( "File already exists. Set forceReplace as true to override the file." );
            else
                File.WriteAllText( $"{path}.cs", generatedCode );
            return generatedCode;
        }
    }
}