﻿namespace Code {

    public abstract class Builder {

        public abstract string ToFile( string path, bool forceReplace = false );
    }
}