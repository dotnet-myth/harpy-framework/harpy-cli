﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using Testura.Code;
using Testura.Code.Generators.Common;
using Testura.Code.Statements;

namespace Code {

    public class Logic {
        public List<StatementSyntax> Logics { get; set; }

        public Logic( ) {
            Logics = new List<StatementSyntax>( );
        }

        public Logic WithVariable( string name, Action<Variable> action ) {
            var variable = new Variable( name );
            action.Invoke( variable );
            Logics.Add( variable.ToBuilder( ) );
            return this;
        }

        public Logic WithStatments( params StatementSyntax[ ] statements ) {
            Logics.AddRange( statements );
            return this;
        }

        public Logic WithReturn( Reference reference ) {
            Logics.Add( new JumpStatement( ).Return( reference.BuildExpression( ) ) );
            return this;
        }

        public Logic With( Reference reference ) {
            Logics.Add( new ExpressionStatement( ).Invoke( reference.Build( ) ).AsStatement( ) );
            return this;
        }

        public Logic WithCustom( string line, params Argument[ ] arguments ) {
            Logics.Add( new ExpressionStatement( ).Invoke( line, arguments.Build( ) ).AsStatement( ) );
            return this;
        }

        public IEnumerable<StatementSyntax> ToBuilder( ) {
            return Logics;
        }

        public Logic WithIf( Argument left, Argument right, ConditionalStatements conditionalType, Action<Logic> action ) {
            var logic = new Logic( );
            action.Invoke( logic );
            var body = BodyGenerator.Create( logic.ToBuilder( ).ToArray( ) );
            Logics.Add( new SelectionStatement( ).If( left.Build( ), right.Build( ), conditionalType, body ) );
            return this;
        }
    }
}