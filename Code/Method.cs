﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using Testura.Code;
using Testura.Code.Builders;
using Testura.Code.Generators.Common;
using Testura.Code.Models;
using Testura.Code.Models.Types;
using Attribute = Testura.Code.Models.Attribute;

namespace Code {

    public class Method : IMethodReference {
        public string Name { get; private set; }

        public List<Argument> Arguments { get; private set; }

        private List<Modifiers> Modifiers { get; set; }

        public List<Parameter> Parameters { get; private set; }

        public Logic Logics { get; private set; }

        public Type ReturnType { get; private set; }

        private List<Attribute> Attributes { get; set; }

        public Method( string name ) {
            Name = name;
            Arguments = new List<Argument>( );
            Parameters = new List<Parameter>( );
            Modifiers = new List<Modifiers>( );
            ReturnType = CustomType.Create( "void" );
            Attributes = new List<Attribute>( );
        }

        public Method WithArguments( params Argument[ ] arguments ) {
            Arguments.AddRange( arguments );
            return this;
        }

        public Method WithArguments( params string[ ] arguments ) {
            Arguments.AddRange( arguments.Select( argument => Argument.Value( argument ) ) );
            return this;
        }

        public Method WithModifiers( params Modifiers[ ] modifiers ) {
            Modifiers.AddRange( modifiers );
            return this;
        }

        public Method WithParameter( string name, Type type, ParameterModifiers modifier = ParameterModifiers.None ) {
            Parameters.Add( new Parameter( name, type, modifier ) );
            return this;
        }

        public Method WithParameter( string name, string type, ParameterModifiers modifier = ParameterModifiers.None ) {
            Parameters.Add( new Parameter( name, CustomType.Create( type ), modifier ) );
            return this;
        }

        public Method WithReturnType( Type type ) {
            ReturnType = type;
            return this;
        }

        public Method WithReturnType( string type ) {
            ReturnType = CustomType.Create( type );
            return this;
        }

        public Method WithLogic( Action<Logic> action ) {
            var logic = new Logic( );
            action.Invoke( logic );
            Logics = logic;
            return this;
        }

        public Method WithAttribute( string name, params Argument[ ] arguments ) {
            Attributes.Add( new Attribute( name, arguments.Build( ).ToList( ) ) );
            return this;
        }

        public Method WithAttributes( params Attribute[ ] attributes ) {
            Attributes.AddRange( attributes );
            return this;
        }

        public MethodDeclarationSyntax ToBuilder( ) {
            var builder = new MethodBuilder( Name )
                .WithModifiers( Modifiers.ToArray( ) )
                .WithParameters( Parameters.ToArray( ) )
                .WithReturnType( ReturnType );

            if ( Attributes.Any( ) )
                builder.WithAttributes( Attributes.ToArray( ) );

            if ( Logics != null )
                builder.WithBody( BodyGenerator.Create( Logics.ToBuilder( ).ToArray( ) ) );

            return builder.Build( );
        }

        public static Method Create( string name, params Argument[ ] arguments ) {
            var method = new Method( name );
            return method
                .WithArguments( arguments );
        }
    }
}