﻿using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Code {

    public interface ILogic {

        StatementSyntax ToBuilder( );
    }
}