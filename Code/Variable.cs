﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using Testura.Code.Models.Types;
using Testura.Code.Statements;

namespace Code {

    public class Variable : ILogic {
        public string Name { get; private set; }

        private Reference ReferenceMember { get; set; }

        private Type ReferenceType { get; set; }

        private System.Type DeclareType { get; set; }

        public enum Type { Reference, Assign, Declare }

        public Variable( ) {
        }

        public Variable( string name ) {
            Name = name;
        }

        public Variable AssignAs( string variable ) {
            ReferenceMember = Reference.Variable( variable );
            ReferenceType = Type.Assign;
            return this;
        }

        public Variable AssignAs( Reference reference ) {
            ReferenceMember = reference;
            ReferenceType = Type.Assign;
            return this;
        }

        public Variable ReferenceTo( Reference reference ) {
            ReferenceMember = reference;
            ReferenceType = Type.Reference;
            return this;
        }

        public Variable AssignAs( string variable, string method, params Argument[ ] arguments ) {
            ReferenceMember = Reference.Chained(
                Reference.Variable( variable ),
                Reference.Method( method, arguments ) );
            ReferenceType = Type.Assign;
            return this;
        }

        public Variable AssignAs( string method, params Argument[ ] arguments ) {
            ReferenceMember = Reference.Method( method, arguments );
            ReferenceType = Type.Assign;
            return this;
        }

        public Variable ReferenceTo( string method, params Argument[ ] arguments ) {
            var member = Reference.Variable( Name );
            member.References.Add( Reference.Method( method, arguments ) );
            ReferenceMember = member;
            ReferenceType = Type.Reference;
            return this;
        }

        public Variable WithDeclarationType( string type = "var" ) {
            WithDeclarationType( CustomType.Create( type ) );
            return this;
        }

        public Variable WithDeclarationType( System.Type type ) {
            DeclareType = type;
            return this;
        }

        public StatementSyntax ToBuilder( ) {
            if ( ReferenceType == Type.Assign ) {
                if ( DeclareType == null )
                    return Statement.Declaration.Assign( Name, ReferenceMember.BuildExpression( ) );
                else
                    return Statement.Declaration.DeclareAndAssign( Name, DeclareType, ReferenceMember.BuildExpression( ) );
            } else
                return new ExpressionStatement( ).Invoke( ReferenceMember.Build( ) ).AsStatement( );
        }
    }
}