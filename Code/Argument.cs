﻿using System.Collections.Generic;
using System.Linq;
using Testura.Code;
using Testura.Code.Generators.Common.Arguments.ArgumentTypes;
using Testura.Code.Generators.Common.BinaryExpressions;
using Testura.Code.Models.References;

namespace Code {

    public class Argument {
        public string VariableName { get; private set; }

        public string MemberName { get; private set; }

        public List<Argument> Arguments { get; private set; }

        public Reference Reference { get; private set; }

        public Reference Left { get; private set; }

        public Reference Right { get; private set; }

        public ConditionalStatements Conditional { get; private set; }

        private ArgumentType Type { get; set; }

        public enum ArgumentType { Value, Lambda, Method, Chained, Variable }

        public Argument( string memberName, ArgumentType type ) {
            MemberName = memberName;
            Type = type;
            Arguments = new List<Argument>( );
        }

        public Argument( string memberName, Reference left, Reference right, ConditionalStatements conditional, ArgumentType type ) : this( memberName, type ) {
            Left = left;
            Right = right;
            Conditional = conditional;
        }

        public Argument( string memberName, Reference reference, ArgumentType type ) : this( memberName, type ) {
            Reference = reference;
        }

        public Argument( string memberName, ArgumentType type, List<Argument> arguments ) : this( memberName, type ) {
            Arguments = arguments;
        }

        public Argument( string variable, string memberName, ArgumentType type, List<Argument> arguments ) : this( memberName, type, arguments ) {
            VariableName = variable;
        }

        public static Argument Lambda( string variable, Reference reference, bool continueFromLambdaMember = true ) {
            var referenceMember = reference;
            if ( continueFromLambdaMember ) {
                referenceMember = Reference.Member( variable, reference.Assign );
                referenceMember.References = new List<Reference>( reference.References );
                referenceMember.References.Insert( 0, reference );
            }
            return new Argument( "\t\t" + variable, referenceMember, ArgumentType.Lambda );
        }

        public static Argument Lambda( string variable, Reference leftReference, ConditionalStatements condition, Reference rightReference, bool continueFromLambdaMember = true ) {
            var referenceMember = leftReference;
            if ( continueFromLambdaMember ) {
                referenceMember = Reference.Member( variable, leftReference.Assign );
                referenceMember.References = new List<Reference>( leftReference.References );
                referenceMember.References.Insert( 0, leftReference );
            }
            return new Argument( variable, referenceMember, rightReference, condition, ArgumentType.Lambda );
        }

        public static Argument Member( string variable, string method, params Argument[ ] arguments ) {
            return new Argument( variable, method, ArgumentType.Method, arguments.ToList( ) );
        }

        public static Argument Value( string value ) {
            return new Argument( value, ArgumentType.Value );
        }

        public static Argument Variable( string value ) {
            return new Argument( value, ArgumentType.Variable );
        }

        public IArgument Build( ) {
            return Type switch {
                ArgumentType.Lambda => BuildLambda( ),

                ArgumentType.Method => new ReferenceArgument(
                    new VariableReference( VariableName,
                       new MethodReference( MemberName, Arguments.Build( ) ) ) ),

                ArgumentType.Variable => new VariableArgument( MemberName ),

                _ => new ValueArgument( MemberName ),
            };
        }

        private IArgument BuildLambda( ) {
            var lambdaVariable = "\r\n\t\t" + MemberName;
            if ( Reference != null )
                return new LambdaArgument( Reference.BuildExpression( ), lambdaVariable );
            else
                return new LambdaArgument( new ConditionalBinaryExpression(
                    Left.BuildExpression( ),
                    Right.BuildExpression( ),
                    Conditional ).GetBinaryExpression( ),
                    lambdaVariable );
        }
    }

    public static class ArgumentExtension {

        public static IEnumerable<IArgument> Build( this IEnumerable<Argument> arguments ) {
            return arguments.Select( x => x.Build( ) );
        }
    }
}