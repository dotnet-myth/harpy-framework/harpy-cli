﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using Testura.Code;
using Testura.Code.Generators.Class;
using Testura.Code.Generators.Common;
using Testura.Code.Generators.Common.Arguments.ArgumentTypes;
using Testura.Code.Models;
using Testura.Code.Models.Types;

namespace Code {

    public class Constructor {
        private string ClassName { get; set; }

        public List<Modifiers> Modifiers { get; private set; }

        public Logic Logics { get; private set; }

        public List<Parameter> Parameters { get; private set; }

        public List<VariableArgument> BaseArguments { get; private set; }

        public Constructor( string className ) {
            ClassName = className;
            Modifiers = new List<Modifiers>( );
            Logics = new Logic( );
            Parameters = new List<Parameter>( );
            BaseArguments = new List<VariableArgument>( );
        }

        public Constructor( string className, params Modifiers[ ] modifiers ) : this( className ) {
            Modifiers.AddRange( modifiers );
        }

        public Constructor WithModifiers( params Modifiers[ ] modifiers ) {
            Modifiers.AddRange( modifiers );
            return this;
        }

        public Constructor WithParameter( string name, Type type, ParameterModifiers modifier = ParameterModifiers.None ) {
            Parameters.Add( new Parameter( name, type, modifier ) );
            return this;
        }

        public Constructor WithParameter( string name, string type, ParameterModifiers modifier = ParameterModifiers.None ) {
            Parameters.Add( new Parameter( name, CustomType.Create( type ), modifier ) );
            return this;
        }

        public Constructor WithParameters( params Parameter[ ] parameters ) {
            Parameters.AddRange( parameters );
            return this;
        }

        public Constructor WithLogic( Action<Logic> action ) {
            var logic = new Logic( );
            action.Invoke( logic );

            Logics = logic;

            return this;
        }

        public Constructor WithBase( string variable ) {
            BaseArguments.Add( new VariableArgument( variable ) );

            return this;
        }

        public ConstructorDeclarationSyntax ToBuilder( ) {
            ConstructorInitializer initializer = null;
            if ( BaseArguments.Any( ) )
                initializer = new ConstructorInitializer( ConstructorInitializerTypes.Base, BaseArguments );

            var constructor = ConstructorGenerator.Create(
                ClassName,
                BodyGenerator.Create( Logics.ToBuilder( ).ToArray( ) ),
                Parameters,
                Modifiers,
                constructorInitializer: initializer );

            return constructor;
        }
    }
}