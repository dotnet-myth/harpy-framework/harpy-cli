﻿using System.IO;
using System.Linq;

namespace Harpy.Cli.Extensions
{

    public static class DirectoryExtension {

        public static bool ContainsSubdirectory( this DirectoryInfo directory, string subdirectory ) =>
             directory.GetDirectories( subdirectory ).Any( );
    }
}
