﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;
using Testura.Code;
using Testura.Code.Generators.Common;
using Testura.Code.Generators.Common.Arguments.ArgumentTypes;
using Testura.Code.Models;
using Testura.Code.Models.Properties;
using Testura.Code.Models.References;
using Testura.Code.Statements;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Extensions
{

    public static class NameTypeExtension {

        public static Property[ ] AsProperties( this IEnumerable<NameType> nameTypes, bool privateSet = true ) {
            if ( nameTypes == null )
                return new List<Property>( ).ToArray( );

            return nameTypes.Select( x => {
                var setModifieres = new List<Modifiers>( );

                if ( privateSet )
                    setModifieres.Add( Modifiers.Private );

                return new AutoProperty(
                     name: x.Name,
                     type: x.Type,
                     propertyType: PropertyTypes.GetAndSet,
                     modifiers: new List<Modifiers> { Modifiers.Public },
                     setModifiers: setModifieres );
            } ).ToArray( );
        }

        public static Property[ ] AsReaOnlyList( this IEnumerable<NameType> nameTypes ) {
            if ( nameTypes == null )
                return new List<Property>( ).ToArray( );

            return nameTypes.Select( x => {
                return new BodyProperty(
                        name: x.Name,
                        type: x.Enumerable,
                        getBody: BodyGenerator.Create( Statement.Expression.Invoke( $"{x.Name.ToFieldCase( )}.AsReadOnly" ).AsStatement( ) ),
                        setBody: null,
                        modifiers: new List<Modifiers> { Modifiers.Public } );
            } ).ToArray( );
        }

        public static Field[ ] AsFields( this IEnumerable<NameType> nameTypes ) {
            if ( nameTypes == null )
                return new List<Field>( ).ToArray( );

            return nameTypes.Select( x => {
                return new Field(
                    name: x.Name.ToFieldCase( ),
                    type: x.List,
                    modifiers: new List<Modifiers> { Modifiers.Private, Modifiers.Readonly },
                    initializeWith: Statement.Expression.Invoke( $"new {TypeGenerator.Create( x.List )}" ).AsExpression( ) );
            } ).ToArray( );
        }

        public static Field[ ] AsRepositories( this IEnumerable<NameType> list ) {
            if ( list == null )
                return new List<Field>( ).ToArray( );

            return list.Select( x => {
                return new Field(
                    name: x.Name.ToFieldCase( ) + "Repository",
                    type: x.Custom( "I", "Repository" ),
                    modifiers: new List<Modifiers> { Modifiers.Private, Modifiers.Readonly } );
            } ).ToArray( );
        }

        public static Field[ ] AsQueries( this IEnumerable<NameType> list ) {
            if ( list == null )
                return new List<Field>( ).ToArray( );

            return list.Select( x => {
                return new Field(
                    name: x.Name.ToFieldCase( ) + "Query",
                    type: x.Custom( "I", "Query" ),
                    modifiers: new List<Modifiers> { Modifiers.Private, Modifiers.Readonly } );
            } ).ToArray( );
        }

        public static Parameter[ ] AsParameters( this IEnumerable<NameType> nameTypes, string sufix = default ) {
            if ( nameTypes == null )
                return new List<Parameter>( ).ToArray( );

            return nameTypes.Select( x => {
                var name = x.Name.ToCamelCase( ) + sufix;
                var type = x.Custom( sufix != default ? "I" : "", sufix );
                return new Parameter( name, type );
            } ).ToArray( );
        }

        public static ExpressionStatementSyntax[ ] AsStatements( this IEnumerable<NameType> nameTypes, string sufix = default ) {
            if ( nameTypes == null )
                return new List<ExpressionStatementSyntax>( ).ToArray( );

            return nameTypes.Select( x => {
                var name = x.Name;
                return Statement.Declaration.Assign(
                    sufix == default ? name.ToFirstUpper( ) : name.ToFieldCase( ) + sufix,
                    ReferenceGenerator.Create( new VariableReference( name.ToCamelCase( ) + sufix ) ) );
            } ).ToArray( );
        }

        public static ExpressionStatementSyntax[ ] AsMap( this IEnumerable<NameType> nameTypes ) {
            if ( nameTypes == null )
                return new List<ExpressionStatementSyntax>( ).ToArray( );

            return nameTypes.Select( x => {
                var name = x.Name;
                return new ExpressionStatement( )
                    .Invoke( new VariableReference( "builder",
                             new MemberReference( $"Property( \r\n\t\tcolumn=> column.{name} )",
                             new MethodReference( $"HasColumnName", new List<Argument> { new ValueArgument( Names.Table( name ) ) } ) ) ) )
                             .AsStatement( );
            } ).ToArray( );
        }

        public static StatementSyntax[ ] AsMethodDeclaration( this IEnumerable<NameType> nameTypes ) {
            return nameTypes.Select( val =>
                new ExpressionStatement( ).Invoke( val.Name.ToFirstUpper( ) ).AsStatement( )
            ).ToArray( );
        }
    }
}
