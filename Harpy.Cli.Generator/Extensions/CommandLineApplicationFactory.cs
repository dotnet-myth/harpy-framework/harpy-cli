﻿using McMaster.Extensions.CommandLineUtils;
using System;
using Harpy.Cli.Extensions;
using Harpy.Cli.Parsers;

namespace Harpy.Cli.Extensions
{

    public class CommandLineApplicationFactory {

        public static CommandLineApplication Create( IServiceProvider services ) {
            Print.Configure( services );

            var app = new CommandLineApplication<Program>( );
            app.ValueParsers.Add( new NameTypeParser( ) );
            app.Conventions.UseDefaultConventions( );
            app.Conventions.UseConstructorInjection( services );
            return app;
        }
    }
}
