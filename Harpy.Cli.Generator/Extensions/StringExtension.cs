﻿using Myth.Extensions;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Extensions
{

    public static class StringExtension {

        public static string ToCamelCase( this string text ) {
            if ( text.Any( ) )
                return Char.ToLowerInvariant( text.First( ) ) + text.Substring( 1 );
            return text;
        }

        public static string ToCamelCase( this Names text ) {
            return text.Description.ToCamelCase( );
        }

        public static string ToFirstUpper( this string text ) {
            if ( text.Any( ) )
                return Char.ToUpperInvariant( text.First( ) ) + text.Substring( 1 );
            return text;
        }

        public static string ToFieldCase( this string text ) {
            if ( text.Any( ) )
                return "_" + Char.ToLowerInvariant( text.First( ) ) + text.Substring( 1 );
            return text;
        }

        public static string ToFieldCase( this Names text ) {
            return text.Description.ToFieldCase( );
        }

        public static string Minify( this string text ) {
            return Regex.Replace( text, @"\s+", "" );
        }

        public static string RemoveAttributes( this string line ) {
            if ( line.Contains( "[" ) && line.Contains( "]" ) ) {
                var attribute = line.GetStringBetween( '[', ']' );
                while ( attribute != "" ) {
                    line = line.Replace( attribute, "" );
                    attribute = line.GetStringBetween( '[', ']' );
                }

                line = line
                    .Replace( "[", "" )
                    .Replace( "]", "" )
                    .Trim( );

                if ( line.LastOrDefault( ) == ',' )
                    line = line.Remove( line.Length - 1 );
            }
            return line;
        }
    }
}
