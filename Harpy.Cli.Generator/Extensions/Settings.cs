﻿using Harpy.Cli.ValueObjects;
using Newtonsoft.Json;
using System;
using System.IO;

namespace Harpy.Cli.Extensions {

    public static class Settings {

        public static void LoadPackageVersions( ) {
            var settingsDirectory = new DirectoryInfo(
                Path.Combine( Environment.GetFolderPath(
                    Environment.SpecialFolder.ApplicationData ), "Harpy", "Cli" ) );

            var packagePath = Path.Combine( settingsDirectory.FullName, "package-versions.json" );
            if ( !settingsDirectory.Exists ) {
                settingsDirectory.Create( );
            }

            if ( !File.Exists( packagePath ) )
                File.WriteAllText( packagePath, Resources.PackageVersions );

            var file = File.ReadAllText( packagePath );
            var packageVersions = JsonConvert.DeserializeObject<PackageVersions>( file );

            Using.Packages = packageVersions;
        }
    }
}