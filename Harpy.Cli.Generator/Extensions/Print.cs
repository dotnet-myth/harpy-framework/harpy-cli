﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Harpy.Cli.Directories;

namespace Harpy.Cli.Extensions
{

    public static class Print {
        private static IDirectoryResolver _directoryResolver;

        public static void Configure( IServiceProvider serviceProvider ) {
            _directoryResolver = serviceProvider.GetRequiredService<IDirectoryResolver>( );
        }

        public static void From( string file ) {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine( "\t{0,6}  {1}", "from", file );
            Console.ResetColor( );
        }

        public static void Create( string file ) {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine( "\t{0,6}  {1}", "create", file.Replace( _directoryResolver.GetCurrentDirectory( ).FullName + "\\", "" ) );
            Console.ResetColor( );
        }

        public static void Error( string message ) {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine( "Error\t{0}", message );
            Console.ResetColor( );
        }
    }
}
