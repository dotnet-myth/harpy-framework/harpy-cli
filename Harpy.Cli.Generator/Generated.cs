﻿using Code;
using System;
using System.Collections.Generic;

namespace Harpy.Cli
{

    public class Generated {
        public static IDictionary<Type, Builder> LastGenerated { get; set; } = new Dictionary<Type, Builder>( );

        public static bool IsSetted<T>( ) {
            return LastGenerated[ typeof( T ) ] != null;
        }
    }
}
