﻿using McMaster.Extensions.CommandLineUtils.Abstractions;
using System;
using System.Globalization;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Parsers
{

    public class NameTypeParser : IValueParser {

        public NameTypeParser( ) {
        }

        public Type TargetType { get; } = typeof( NameType );

        public object Parse( string argName, string value, CultureInfo culture ) {
            return Parse( value );
        }

        public static NameType Parse( string value ) {
            var splited = value.Split( ":" );
            var name = splited[ 0 ];
            var type = splited.Length == 1 ? splited[ 0 ] : splited[ 1 ];

            return new NameType( name, type );
        }
    }
}
