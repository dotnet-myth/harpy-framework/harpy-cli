﻿using Harpy.Cli.Commands;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli.Commands.NewCommands;
using Harpy.Cli.Commands.ScaffoldCommands;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Harpy.Cli {

    [Command( UnrecognizedArgumentHandling = UnrecognizedArgumentHandling.StopParsingAndCollect )]
    [Subcommand(
        typeof( GenerateCommand ),
        typeof( NewCommand ),
        typeof( ScaffoldCommand ),
        typeof( VersionCommand )
    )]
    [HelpOption( )]
    public class Program {

        private void OnExecute( CommandLineApplication app ) {
            app.ShowHelp( );
        }

        public static int Main( string[ ] args ) {
            try {
                Settings.LoadPackageVersions( );

                var services = new ServiceCollection( )
                    .AddSingleton<IDirectoryResolver>( new DirectoryResolver( ) )
                    .BuildServiceProvider( );

                var app = CommandLineApplicationFactory.Create( services );

                return app.Execute( args );
            } catch ( Exception e ) {
                Print.Error( e.Message );
            }

            return -1;
        }
    }
}