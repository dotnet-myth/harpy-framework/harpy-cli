﻿using System.IO;

namespace Harpy.Cli.Directories
{

    public interface IDirectoryResolver {

        bool ContainsProjectFile( DirectoryInfo directory );

        bool ContainsSolution( DirectoryInfo directory );

        bool ContainsSubdirectory( DirectoryInfo directory, string subdirectory );

        DirectoryInfo GetCurrentDirectory( );

        string GetProjectName( );

        bool IsProjectDirectory( DirectoryInfo directory );

        bool IsServiceDirectory( DirectoryInfo directory );

        DirectoryInfo SetCurrentDirectory( string directory );
    }
}
