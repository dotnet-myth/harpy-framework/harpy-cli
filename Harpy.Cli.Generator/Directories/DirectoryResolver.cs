﻿using System;
using System.IO;
using System.Linq;
using Harpy.Cli.Directories;

namespace Harpy.Cli.Directories
{

    public class DirectoryResolver : IDirectoryResolver {

        public DirectoryInfo GetCurrentDirectory( ) =>
            new DirectoryInfo( Directory.GetCurrentDirectory( ) );

        public bool ContainsProjectFile( DirectoryInfo directory ) =>
            directory.GetFiles( "*.csproj" ).Any( );

        public bool ContainsSolution( DirectoryInfo directory ) =>
             directory.GetFiles( "*.sln" ).Any( );

        public bool ContainsSubdirectory( DirectoryInfo directory, string subdirectory ) =>
             directory.GetDirectories( subdirectory ).Any( );

        public bool IsServiceDirectory( DirectoryInfo directory ) {
            if ( directory.Parent == null )
                return false;
            return ContainsSolution( directory.Parent );
        }

        public bool IsProjectDirectory( DirectoryInfo directory ) {
            return ContainsSolution( directory );
        }

        public DirectoryInfo SetCurrentDirectory( string directory ) {
            Directory.SetCurrentDirectory( directory );
            return new DirectoryInfo( directory );
        }

        public string GetProjectName( ) {
            var directories = GetCurrentDirectory( ).EnumerateFiles( "*.sln", SearchOption.AllDirectories );
            var solutionFile = directories.FirstOrDefault( x => x.Extension == ".sln" );
            if ( solutionFile != null )
                return solutionFile.Name.Replace( ".sln", "" );

            throw new Exception( "Solution file not found!" );
        }
    }
}
