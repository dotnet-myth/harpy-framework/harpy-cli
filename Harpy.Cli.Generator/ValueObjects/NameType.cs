﻿using System;
using Testura.Code.Models.Types;
using Harpy.Cli.Extensions;

namespace Harpy.Cli.ValueObjects
{

    public class NameType {
        public string Name { get; private set; }

        public string TypeName { get; private set; }

        public Type Type => CustomType.Create( TypeName );

        public Type Interface => CustomType.Create( string.Concat( "I", TypeName ) );

        public Type Enumerable => CustomType.Create( string.Concat( "IEnumerable<", TypeName, ">" ) );

        public Type List => CustomType.Create( string.Concat( "List<", TypeName, ">" ) );

        public Type Custom( string prefix = default, string sufix = default ) => CustomType.Create( string.Concat( prefix, TypeName, sufix ) );

        public NameType( ) {
        }

        public NameType( string name, string typeName ) {
            Name = name.ToFirstUpper( );
            TypeName = typeName;
        }
    }
}
