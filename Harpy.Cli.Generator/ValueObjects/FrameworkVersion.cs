﻿using Myth.ValueObjects;
using System.Collections.Generic;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.ValueObjects
{

    public class FrameworkVersion : ValueObject {
        public string Description { get; private set; }

        public FrameworkVersion( string description ) {
            Description = description;
        }

        protected override IEnumerable<object> GetAtomicValues( ) {
            yield return Description;
        }

        public static implicit operator string( FrameworkVersion version ) => version.Description;

        public override string ToString( ) => Description;

        public string ToTargetFramework( ) => $"<TargetFramework>{Description}</TargetFramework>";

        public static FrameworkVersion NetCore3_1 => new FrameworkVersion( "netcoreapp3.1" );

        public static FrameworkVersion Net6 => new FrameworkVersion( "net6.0" );
    }
}
