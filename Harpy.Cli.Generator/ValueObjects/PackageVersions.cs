﻿using System.Collections.Generic;
using System.Linq;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.ValueObjects
{
    public class PackageVersions {
        public List<Package> Packages { get; set; } = new List<Package>( );

        public bool Contains( string packageName ) => Packages.Exists( x => x.Name == packageName );

        public string GetVersion(string packageName ) => Packages.FirstOrDefault(x => x.Name == packageName).Version;
    }

    public class Package {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}
