﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.ValueObjects
{

    public class Project {
        public string ServiceName { get; private set; }

        public ServiceType Service { get; private set; }

        public ProjectType Type { get; private set; }

        public FileInfo CurrentFile { get; private set; }

        private Project( ProjectType type ) {
            Type = type;
        }

        public Project( ServiceType service ) : this( ProjectType.Library ) {
            Service = service;
        }

        public Project( string serviceName, ServiceType service ) : this( ProjectType.Library ) {
            ServiceName = serviceName.ToFirstUpper( );
            Service = service;
        }

        public Project( string serviceName, ServiceType service, ProjectType type ) : this( type ) {
            ServiceName = serviceName.ToFirstUpper( );
            Service = service;
        }

        public enum ServiceType { Domain, Application, IoC, Context, Repository, Resource, DomainTest, PresentationTest, AntiCorruptionLayer, Presentation, Migration }

        public enum ProjectType { Library, Test }

        public void Create( string path ) {
            if ( !File.Exists( path ) ) {
                var directory = new DirectoryInfo( Path.Combine( path, ServiceName ) );
                if ( !directory.Exists )
                    directory.Create( );

                new Batch( @$"dotnet new ""{GetProjectType( )}"" --no-restore -f {FrameworkVersion.Net6} -o ""{directory.FullName}\{GetServiceName( )}"" -n {GetServiceName( )}" ).Run( );

                var serviceDirectory = new DirectoryInfo( Path.Combine( directory.FullName, GetServiceName( ) ) );

                serviceDirectory
                    .GetFiles( "*.cs" )
                    .ToList( )
                    .ForEach( file => file.Delete( ) );

                CurrentFile = new FileInfo( Path.Combine( serviceDirectory.FullName, GetServiceName( ) + ".csproj" ) );
            } else {
                CurrentFile = new FileInfo( path );
            }
        }

        public void AddReference( params Project[ ] references ) {
            new Batch( @$"dotnet add ""{CurrentFile.FullName}"" reference {string.Join( " ", references.Select( x => $@"""{x.CurrentFile.FullName}""" ) )}" ).Run( );
        }

        public void AddPackages( params Using[ ] packages ) {
            foreach ( var package in packages )
                new Batch( @$"dotnet add ""{CurrentFile.FullName}"" package {package.Description} {( !string.IsNullOrEmpty( package.Version ) ? "-v " + package.Version : "" )} -n" ).Run( );
        }

        public void AddFolders( params string[ ] folders ) {
            var directory = CurrentFile.Directory;

            var itens = new StringBuilder( );
            itens.AppendLine( "\t<ItemGroup>" );

            foreach ( var folder in folders ) {
                if ( !directory.ContainsSubdirectory( folder ) )
                    directory.CreateSubdirectory( folder );

                itens.AppendLine( $"\t\t<Folder Include=\"{folder}\\\" />" );
            }
            itens.AppendLine( "\t</ItemGroup>" );

            var projectText = File.ReadAllLines( CurrentFile.FullName ).ToList( );
            projectText.Insert( projectText.Count - 1, itens.ToString( ) );
            File.WriteAllLines( CurrentFile.FullName, projectText );
        }

        private string GetServiceName( bool isFull = false ) {
            var name = Enum.GetName( typeof( ServiceType ), Service );

            return Service switch {
                ServiceType.IoC => $"{ServiceName.ToFirstUpper( )}{( isFull ? ".Infrastructure.CrossCutting" : "" )}.{name}",
                ServiceType.Context or
                ServiceType.Repository or
                ServiceType.AntiCorruptionLayer or
                ServiceType.Resource => $"{ServiceName.ToFirstUpper( )}{( isFull ? ".Infrastructure.Data" : "" )}.{name}",
                ServiceType.PresentationTest => $"{ServiceName.ToFirstUpper( )}.Test.Presentation",
                ServiceType.DomainTest => $"{ServiceName.ToFirstUpper( )}.Test.Domain",
                _ => $"{ServiceName.ToFirstUpper( )}.{name}",
            };
        }

        private string GetProjectType( ) {
            return Type switch {
                ProjectType.Test => "xUnit",
                _ => "classlib",
            };
        }

        public string GetSolutionSubDirectory( ) {
            return GetServiceName( true );
        }
    }
}
