﻿using System.Diagnostics;

namespace Harpy.Cli.ValueObjects
{

    public class Batch {
        public string Command { get; private set; }

        public string Directory { get; private set; }

        public Batch( string command ) {
            Command = command;
        }

        public Batch( string command, string directory ) {
            Command = command;
            Directory = directory;
        }

        public string Run( ) {
            var cmd = new Process( );
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            if ( !string.IsNullOrEmpty( Directory ) )
                cmd.StartInfo.WorkingDirectory = Directory;
            cmd.Start( );

            cmd.StandardInput.WriteLine( Command );
            cmd.StandardInput.Flush( );
            cmd.StandardInput.Close( );
            cmd.WaitForExit( );

            return cmd.StandardOutput.ReadToEnd( );
        }
    }
}
