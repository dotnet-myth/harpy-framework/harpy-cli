﻿using System.IO;
using System.Linq;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.ValueObjects
{

    public class Solution {
        public string ProjectName { get; private set; }

        public FileInfo CurrentFile { get; private set; }

        public Solution( string projectName ) {
            ProjectName = projectName.ToFirstUpper( );
        }

        public DirectoryInfo Create( string path ) {
            if ( !File.Exists( path ) ) {
                var directory = new DirectoryInfo( Path.Combine( path, ProjectName ) );

                if ( !directory.Exists )
                    directory.Create( );

                new Batch( $"dotnet new sln -o \"{directory.FullName}\"" ).Run( );

                CurrentFile = new FileInfo( Path.Combine( directory.FullName, $"{ProjectName}.sln" ) );
                return directory;
            } else {
                CurrentFile = new FileInfo( path );
                return CurrentFile.Directory;
            }
        }

        public void AddProject( Project project, string subDirectory = default ) {
            if ( subDirectory != default ) {
                var folders = subDirectory
                    .Replace( ".", @"\" )
                    .Split( @"\" )
                    .ToList( );

                if ( folders.Count >= 4 )
                    folders.RemoveAt( folders.Count - 1 );
                subDirectory = Path.Combine( folders.ToArray( ) );
            }

            new Batch( @$"dotnet sln ""{CurrentFile.FullName}"" add ""{project.CurrentFile.FullName}"" { ( subDirectory != default ? @"-s Services\" + subDirectory : "" )} " ).Run( );
        }
    }
}
