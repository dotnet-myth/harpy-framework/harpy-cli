﻿using Myth.ValueObjects;
using System.Collections.Generic;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.ValueObjects
{

    public class Folder : ValueObject {
        public static Folder Api => new Folder( "Presentation" );

        public static Folder PresentationApi => new Folder( "Presentation.Api" );

        public static Folder PresentationMigrations( string project ) => new Folder( $"{project}.Migrations" );

        public static Folder PresentationApplication => new Folder( "Application" );

        public static Folder Controllers => new Folder( "Controllers" );

        public static Folder Mappings => new Folder( "Mappings" );

        public static Folder Entities => new Folder( "Entities" );

        public static Folder ViewModels => new Folder( "ViewModels" );

        public static Folder DomainToViewModel => new Folder( "DomainToViewModel" );

        public static Folder ViewModelToDomain => new Folder( "ViewModelToDomain" );

        public static Folder Constants => new Folder( "Constants" );

        public static Folder Integrations => new Folder( "Integration" );

        public static Folder IntegrationEvents => new Folder( "IntegrationEvents" );

        public static Folder IntegrationEventHandlers => new Folder( "IntegrationEventHandlers" );

        public static Folder IntegrationJobs => new Folder( "IntegrationJobs" );

        public static Folder AggregateModels => new Folder( "AggregateModels" );

        public static Folder ValueObjects => new Folder( "ValueObjects" );

        public static Folder Jobs => new Folder( "Jobs" );

        public static Folder JobHandlers => new Folder( "JobHandlers" );

        public static Folder Events => new Folder( "Events" );

        public static Folder EventHandlers => new Folder( "EventHandlers" );

        public static Folder Commands => new Folder( "Commands" );

        public static Folder CommandHandlers => new Folder( "CommandHandlers" );

        public static Folder Validations => new Folder( "Validations" );

        public static Folder Interfaces => new Folder( "Interfaces" );

        public static Folder Data => new Folder( "Data" );

        public static Folder Context => new Folder( "Context" );

        public static Folder Repository => new Folder( "Repository" );

        public static Folder Repositories => new Folder( "Repositories" );

        public static Folder IRepositories => new Folder( "Interfaces/Repositories" );

        public static Folder IQueries => new Folder( "Interfaces/Queries" );

        public static Folder Queries => new Folder( "Queries" );

        public static Folder Specifications => new Folder( "Specifications" );

        public static Folder CrossCutting => new Folder( "CrossCutting" );

        public static Folder IoC => new Folder( "IoC" );

        public static Folder Migrations => new Folder( "Migrations" );

        public string Description { get; private set; }

        public Folder( string description ) {
            Description = description;
        }

        protected override IEnumerable<object> GetAtomicValues( ) {
            yield return Description;
        }

        public static implicit operator string( Folder name ) => name.Description;

        public static Folder Service( string domain ) => new Folder( $"{domain.ToFirstUpper( )}.Domain" );

        public static Folder Presentation( string domain ) => new Folder( $"{domain.ToFirstUpper( )}.Presentation" );

        public static Folder Application( string domain ) => new Folder( $"{domain.ToFirstUpper( )}.Application" );

        public static Folder Infrastructure( string domain ) => new Folder( $"{domain.ToFirstUpper( )}.Infrastructure" );

        public static Folder Model( string model ) => new Folder( model.ToFirstUpper( ) );
    }
}
