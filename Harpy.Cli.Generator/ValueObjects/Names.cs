﻿using Harpy.Cli.Extensions;
using Myth.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harpy.Cli.ValueObjects {

    public class Names : ValueObject {
        public static Names InjectorContainer => new Names( $"InjectorContainer" );

        public string Description { get; private set; }

        public Names( string description ) {
            Description = description;
        }

        protected override IEnumerable<object> GetAtomicValues( ) {
            yield return Description;
        }

        public static implicit operator string( Names name ) => name.Description;

        public static Names IQuery( string model = "" ) => new Names( $"I{model.ToFirstUpper( )}Query" );

        public static Names Query( string model = "" ) => new Names( $"{model.ToFirstUpper( )}Query" );

        public static Names Specification( string model = "" ) => new Names( $"{model.ToFirstUpper( )}Specification" );

        public static Names IRepository( string model = "" ) => new Names( $"I{model.ToFirstUpper( )}Repository" );

        public static Names Repository( string model = "" ) => new Names( $"{model.ToFirstUpper( )}Repository" );

        public static Names Job( string job ) => new Names( $"{job.ToFirstUpper( )}Job" );

        public static Names JobHandler( string job ) => new Names( $"{job.ToFirstUpper( )}JobHandler" );

        public static Names Event( string @event ) => new Names( $"{@event.ToFirstUpper( )}Event" );

        public static Names EventHandler( string @event ) => new Names( $"{@event.ToFirstUpper( )}EventHandler" );

        public static Names Command( string command ) => new Names( $"{command.ToFirstUpper( )}Command" );

        public static Names Commands( string command ) => new Names( $"{command.ToFirstUpper( )}Commands" );

        public static Names CommandHandler( string command ) => new Names( $"{command.ToFirstUpper( )}CommandHandler" );

        public static Names CommandValidation( string command ) => new Names( $"{command.ToFirstUpper( )}CommandValidation" );

        public static Names Profile( string model ) => new Names( $"{model.ToFirstUpper( )}Profile" );

        public static Names ViewModel( string model ) => new Names( $"{model.ToFirstUpper( )}ViewModel" );

        public static Names Controller( string model ) => new Names( $"{model.ToFirstUpper( )}sController" );

        public static Names Model( string model ) => new Names( $"{model.ToFirstUpper( )}" );

        public static Names Constant( string type ) => new Names( $"Constant<{type.ToFirstUpper( )}>" );

        public static Names Context( string context ) => new Names( $"{context.ToFirstUpper( )}Context" );

        public static Names Map( string model ) => new Names( $"{model.ToFirstUpper( )}Map" );

        public static Names Table( string model ) => new Names( $"{string.Concat( model.Select( x => Char.IsUpper( x ) ? "_" + x : x.ToString( ) ) ).TrimStart( '_' ).ToLower( )}" );

        public static Names Service( string model ) => new Names( $"{model.ToFirstUpper( )}Service" );

        public static Names Migration( string name ) => new Names( name.ToFirstUpper( ) );

        public static Names MigrationFile( string name, DateTime dateTime ) => new Names( $"{dateTime.ToString( "yyyyMMddHHmmss" )}_{name.ToFirstUpper( )}" );

        public override string ToString( ) {
            return Description;
        }
    }
}