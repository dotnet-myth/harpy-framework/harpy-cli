﻿using Harpy.Cli.Extensions;
using Myth.ValueObjects;
using System.Collections.Generic;
using System.Text;

namespace Harpy.Cli.ValueObjects {

    public class Using : ValueObject {
        public static PackageVersions Packages;

        public string Description { get; private set; }

        public string Version { get; private set; }

        private static readonly string _harpyVersion = "2.0.0.5";

        private static readonly string _micorsoftVersion = "6.0.2";

        public Using( string description ) {
            Description = description;
        }

        public Using( string description, string version )
            : this( description ) {
            Version = version;
        }

        public static readonly Using AutoMapper = new Using( "AutoMapper", "11.0.1" );
        public static readonly Using Coverlet_Collector = new Using( "coverlet.collector", "3.1.2" );
        public static readonly Using FluentMigrator = new Using( "FluentMigrator", "3.3.2" );
        public static readonly Using FluentMigrator_Runner = new Using( "FluentMigrator.Runner", "3.3.2" );
        public static readonly Using FluentValidation = new Using( "FluentValidation" );
        public static readonly Using Hangfire = new Using( "Hangfire", "1.7.28" );
        public static readonly Using Harpy_Application = new Using( "Harpy.Application", _harpyVersion );
        public static readonly Using Harpy_Application_CommandHandlers = new Using( "Harpy.Application.CommandHandlers" );
        public static readonly Using Harpy_Application_EventHandlers = new Using( "Harpy.Application.EventHandlers" );
        public static readonly Using Harpy_Application_JobHandlers = new Using( "Harpy.Application.JobHandlers" );
        public static readonly Using Harpy_Context = new Using( "Harpy.Context", _harpyVersion );
        public static readonly Using Harpy_Controller = new Using( "Harpy.Presentation.Controller" );
        public static readonly Using Harpy_Domain = new Using( "Harpy.Domain", _harpyVersion );
        public static readonly Using Harpy_Domain_Commands = new Using( "Harpy.Domain.Commands" );
        public static readonly Using Harpy_Domain_Events = new Using( "Harpy.Domain.Events" );
        public static readonly Using Harpy_Domain_Events_Base = new Using( "Harpy.Domain.Events.Base" );
        public static readonly Using Harpy_Domain_Interfaces_Messages = new Using( "Harpy.Domain.Interfaces.Messages" );
        public static readonly Using Harpy_Domain_Interfaces_Queries = new Using( "Harpy.Domain.Interfaces.Queries.Base" );
        public static readonly Using Harpy_Domain_Interfaces_Repositories_Entity = new Using( "Myth.Interfaces.Repositories.EntityFramework" );
        public static readonly Using Harpy_Domain_Interfaces_Validation = new Using( "Harpy.Domain.Interfaces.Validations" );
        public static readonly Using Harpy_Domain_Jobs_Base = new Using( "Harpy.Domain.Jobs.Base" );
        public static readonly Using Harpy_Domain_Validation = new Using( "Harpy.Domain.Validations" );
        public static readonly Using Harpy_Domain_ValueObjects = new Using( "Harpy.Domain.ValueObjects" );
        public static readonly Using Harpy_Errors = new Using( "Harpy.Domain.Models.Errors" );
        public static readonly Using Harpy_IoC = new Using( "Harpy.IoC", _harpyVersion );
        public static readonly Using Harpy_IoC_Extensions = new Using( "Harpy.IoC.Extensions" );
        public static readonly Using Harpy_IoC_Options = new Using( "Harpy.IoC.Options" );
        public static readonly Using Harpy_Presentation = new Using( "Harpy.Presentation", _harpyVersion );
        public static readonly Using Harpy_Presentation_Extensions = new Using( "Harpy.Presentation.Extensions" );
        public static readonly Using Harpy_Repository = new Using( "Harpy.Repository", _harpyVersion );
        public static readonly Using Harpy_Repository_Entity = new Using( "Myth.Repositories.EntityFramework" );
        public static readonly Using Harpy_SQLite = new Using( "Harpy.SQLite", _harpyVersion );
        public static readonly Using Harpy_SQLite_IoC = new Using( "Harpy.SQLite.IoC" );
        public static readonly Using Harpy_SQLite_Presentation_Extensions = new Using( "Harpy.SQLite.Presentation.Extensions" );
        public static readonly Using Harpy_Test_Domain = new Using( "Harpy.Test.Domain", _harpyVersion );
        public static readonly Using Harpy_Test_Presentation = new Using( "Harpy.Test.Presentation", _harpyVersion );
        public static readonly Using Harpy_Transaction = new Using( "Harpy.Transaction", _harpyVersion );
        public static readonly Using MediatR = new Using( "MediatR" );
        public static readonly Using Microsoft_AspNetCore = new Using( "Microsoft.AspNetCore" );
        public static readonly Using Microsoft_AspNetCore_Authorization = new Using( "Microsoft.AspNetCore.Authorization" );
        public static readonly Using Microsoft_AspNetCore_Builder = new Using( "Microsoft.AspNetCore.Builder" );
        public static readonly Using Microsoft_AspNetCore_Hosting = new Using( "Microsoft.AspNetCore.Hosting" );
        public static readonly Using Microsoft_AspNetCore_Http = new Using( "Microsoft.AspNetCore.Http" );
        public static readonly Using Microsoft_AspNetCore_Mvc = new Using( "Microsoft.AspNetCore.Mvc" );
        public static readonly Using Microsoft_AspNetCore_Mvc_Testing = new Using( "Microsoft.AspNetCore.Mvc.Testing", _micorsoftVersion );
        public static readonly Using Microsoft_AspNetCore_Newtonsoft = new Using( "Microsoft.AspNetCore.Mvc.NewtonsoftJson", _micorsoftVersion );
        public static readonly Using Microsoft_EntityFrameworkCore = new Using( "Microsoft.EntityFrameworkCore", _micorsoftVersion );
        public static readonly Using Microsoft_EntityFrameworkCore_Builders = new Using( "Microsoft.EntityFrameworkCore.Metadata.Builders" );
        public static readonly Using Microsoft_EntityFrameworkCore_Tools = new Using( "Microsoft.EntityFrameworkCore.Tools", _micorsoftVersion );
        public static readonly Using Microsoft_Extensions_Configuration = new Using( "Microsoft.Extensions.Configuration" );
        public static readonly Using Microsoft_Extensions_DependencyInjection = new Using( "Microsoft.Extensions.DependencyInjection" );
        public static readonly Using Microsoft_Extensions_DependencyInjection_Abstraction = new Using( "Microsoft.Extensions.DependencyInjection.Abstractions", "6.0.0" );
        public static readonly Using Microsoft_Extensions_Hosting = new Using( "Microsoft.Extensions.Hosting" );
        public static readonly Using Microsoft_Extensions_Logging = new Using( "Microsoft.Extensions.Logging" );
        public static readonly Using Microsoft_Test_Sdk = new Using( "Microsoft.NET.Test.Sdk", "17.0.0" );
        public static readonly Using Myth_Api = new Using( "Myth.Api" );
        public static readonly Using Myth_Context = new Using( "Myth.Contexts" );
        public static readonly Using Myth_Extensions = new Using( "Myth.Extensions" );
        public static readonly Using Myth_Interfaces = new Using( "Myth.Interfaces" );
        public static readonly Using Myth_Interfaces_Repositories_Results = new Using( "Myth.Interfaces.Repositories.Results" );
        public static readonly Using Myth_Rest = new Using( "Myth.Rest", _harpyVersion );
        public static readonly Using Myth_Specifications = new Using( "Myth.Specifications" );
        public static readonly Using Myth_ValueObjects = new Using( "Myth.ValueObjects" );
        public static readonly Using Nswag_Annotations = new Using( "NSwag.Annotations" );
        public static readonly Using System = new Using( "System" );
        public static readonly Using System_Collections_Generic = new Using( "System.Collections.Generic" );
        public static readonly Using System_Data_Common = new Using( "System.Data.Common" );
        public static readonly Using System_IO = new Using( "System.IO" );
        public static readonly Using System_Linq = new Using( "System.Linq" );
        public static readonly Using System_Net_Http = new Using( "System.Net.Http" );
        public static readonly Using System_Thread = new Using( "System.Threading" );
        public static readonly Using System_Threading_Task = new Using( "System.Threading.Tasks" );
        public static readonly Using XUnit = new Using( "xunit", "2.4.1" );
        public static readonly Using XUnit_DependecyInjection = new Using( "Xunit.DependencyInjection", "8.3.0" );
        public static readonly Using Xunit_Runner_VisualStudio = new Using( "xunit.runner.visualstudio", "2.4.3" );

        public static Using AgreggateModels( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Domain.AggregateModels" );

        public static Using Commands( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Domain.Commands" );

        public static Using Resources( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Resource" );

        public static Using Context( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Infrastructure.Data.Context" );

        public static Using Events( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Domain.Events" );

        public static Using IQueries( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Domain.Interfaces.Queries" );

        public static Using IRepositories( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Domain.Interfaces.Repositories" );

        public static Using Jobs( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Domain.Jobs" );

        public static Using Mappings( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Context.Mappings" );

        public static Using Specification( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Domain.Specifications" );

        public static Using ViewModels( string domain ) => new Using( $"{domain.ToFirstUpper( )}.Presentation.ViewModels" );

        public static Using Presentation( ) => new Using( "Presentation.Api" );

        public string ToPackageReference( ) {
            var version = Version;

            if ( Packages.Contains( Description ) )
                version = Packages.GetVersion( Description );

            return $"<PackageReference Include='{Description}' Version='{version}' />";
        }

        public string ToPackageReferenceWithAssets( ) {
            var version = Version;

            if ( Packages.Contains( Description ) )
                version = Packages.GetVersion( Description );

            var builder = new StringBuilder( );
            builder.AppendLine( $"<PackageReference Include=\"{Description}\" Version=\"{version}\">" );
            builder.AppendLine( "   <PrivateAssets>all</PrivateAssets>" );
            builder.AppendLine( "   <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>" );
            builder.AppendLine( "</PackageReference>" );
            return builder.ToString( );
        }

        public static implicit operator string( Using @using ) => @using.Description;

        protected override IEnumerable<object> GetAtomicValues( ) {
            yield return Description;
        }
    }
}