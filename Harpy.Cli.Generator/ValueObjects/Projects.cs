﻿using Harpy.Cli.Extensions;
using Myth.ValueObjects;
using System.Collections.Generic;

namespace Harpy.Cli.ValueObjects {

    public class Projects : ValueObject {
        public static Projects Api => new Projects( $@"Presentation\Presentation.Api" );

        public string Description { get; private set; }

        public Projects( string description ) {
            Description = description;
        }

        protected override IEnumerable<object> GetAtomicValues( ) {
            yield return Description;
        }

        public static implicit operator string( Projects project ) => project.Description;

        public static Projects Application( string domain ) => new Projects( $"{domain.ToFirstUpper( )}.Application" );

        public static Projects Domain( string domain ) => new Projects( $"{domain.ToFirstUpper( )}.Domain" );

        public static Projects Presentation( string domain ) => new Projects( $"{domain.ToFirstUpper( )}.Presentation" );

        public static Projects IoC( string domain ) => new Projects( $"{domain.ToFirstUpper( )}.IoC" );

        public static Projects Context( string domain ) => new Projects( $"{domain.ToFirstUpper( )}.Context" );

        public static Projects Repository( string domain ) => new Projects( $"{domain.ToFirstUpper( )}.Repository" );

        public static Projects Resource( string domain ) => new Projects( $"{domain.ToFirstUpper( )}.Resource" );

        public static Projects DomainTest( string domain ) => new Projects( $"{domain.ToFirstUpper( )}.Test.Domain" );

        public static Projects Migration( string project ) => new Projects( $"{project.ToFirstUpper( )}.Migrations" );

        public override string ToString( ) => Description;
    }
}