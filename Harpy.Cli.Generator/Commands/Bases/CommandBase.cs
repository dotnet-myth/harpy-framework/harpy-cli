﻿using McMaster.Extensions.CommandLineUtils;
using System.IO;
using System.Linq;
using Harpy.Cli.Directories;

namespace Harpy.Cli.Commands.Bases
{

    public abstract class CommandBase {
        protected IDirectoryResolver _directoryResolver { get; set; }

        protected virtual string FileName { get; }

        protected virtual string[ ] PathPieces { get; }

        public CommandBase( IDirectoryResolver directoryResolver ) {
            _directoryResolver = directoryResolver;
        }

        [Option( Description = "Force override file if exist", Template = "--force" )]
        public bool Force { get; set; }

        public virtual string GeneratePath( string filename, params string[ ] pathPieces ) {
            var path = pathPieces.ToList( );
            path.Insert( 0, _directoryResolver.GetCurrentDirectory( ).FullName );
            var dir = new DirectoryInfo( Path.Combine( path.ToArray( ) ) );
            if ( !dir.Exists )
                dir.Create( );

            return Path.Combine( dir.FullName, filename );
        }

        public abstract int OnExecute( );
    }
}
