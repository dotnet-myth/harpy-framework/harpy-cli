﻿using Code;
using System;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;

namespace Harpy.Cli.Commands.Bases
{

    public abstract class ServiceCommandBase : CommandBase {

        public ServiceCommandBase( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        private string _service;

        public string Service {
            get {
                if ( string.IsNullOrEmpty( _service ) ) {
                    var currentDirectory = _directoryResolver.GetCurrentDirectory( );
                    if ( !_directoryResolver.IsServiceDirectory( currentDirectory ) )
                        throw new Exception( "The current path is not a harpy service!" );
                    _service = currentDirectory.Name.ToFirstUpper( );
                }
                return _service;
            }
        }

        public abstract Builder GenerateCode( );

        public override int OnExecute( ) {
            try {
                var typeName = GetType( ).Name;
                if ( !typeName.Contains( "Implementation" ) && !typeName.Contains( "Interface" ) )
                    Print.From( typeName.Remove( typeName.Length - 7, 7 ) );
                var @class = GenerateCode( );
                Generated.LastGenerated[ GetType( ) ] = @class;
                var path = GeneratePath( FileName, PathPieces );
                @class.ToFile( path, Force );
                Print.Create( $"{path}.cs".Replace( _directoryResolver.GetCurrentDirectory( ).Parent.FullName + @"\", "" ) );
                return 1;
            } catch ( Exception ) {
                throw;
            }
        }
    }
}
