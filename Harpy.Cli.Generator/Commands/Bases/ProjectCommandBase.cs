﻿using Code;
using System;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;

namespace Harpy.Cli.Commands.Bases
{

    public abstract class ProjectCommandBase : CommandBase {

        public ProjectCommandBase( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        private string _project;

        public string Project {
            get {
                if ( string.IsNullOrEmpty( _project ) ) {
                    var currentDirectory = _directoryResolver.GetCurrentDirectory( );
                    if ( !_directoryResolver.IsProjectDirectory( currentDirectory ) )
                        throw new Exception( "The current path is not a harpy project!" );
                    _project = currentDirectory.Name.ToFirstUpper( );
                }
                return _project;
            }
        }

        public abstract Builder GenerateCode( );

        public override int OnExecute( ) {
            try {
                var typeName = GetType( ).Name;
                var @class = GenerateCode( );

                Generated.LastGenerated[ GetType( ) ] = @class;
                Print.From( typeName.Remove( typeName.Length - 7, 7 ) );

                var path = GeneratePath( FileName, PathPieces );
                @class.ToFile( path, Force );

                Print.Create( $"{path}.cs".Replace( _directoryResolver.GetCurrentDirectory( ).Parent.FullName + @"\", "" ) );
                return 1;
            } catch ( Exception ) {
                throw;
            }
        }
    }
}
