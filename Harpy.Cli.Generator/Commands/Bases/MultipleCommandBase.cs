﻿using System;
using System.Collections.Generic;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;

namespace Harpy.Cli.Commands.Bases
{

    public abstract class MultipleCommandBase : CommandBase {
        protected virtual IEnumerable<CommandBase> Commands { get; set; }

        public MultipleCommandBase( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        public override int OnExecute( ) {
            try {
                foreach ( var command in Commands ) {
                    if ( command is MultipleCommandBase multipleCommand )
                        Print.From( multipleCommand.GetType( ).Name.Replace( "Command", "" ) );
                    command.OnExecute( );
                }
                return 1;
            } catch ( Exception ) {
                return -1;
            }
        }
    }
}
