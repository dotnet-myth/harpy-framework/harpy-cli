﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.ComponentModel.DataAnnotations;
using Testura.Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.GenerateCommands.Repository
{

    public class ImplementationCommand : ServiceCommandBase {

        public ImplementationCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-t|--type", Description = "Repository type" )]
        public RepositoryCommand.Type RepositoryType { get; set; } = RepositoryCommand.Type.ReadWrite;

        protected override string FileName => Names.Repository( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Repository( Service ) };

        private string RepositoryBase {
            get {
                var repositoryType = Enum.GetName( typeof( RepositoryCommand.Type ), RepositoryType );
                return $"{ repositoryType }RepositoryAsync<{Names.Model( Name )}>, I{ Names.Model( Name ) }Repository";
            }
        }

        public override Class GenerateCode( ) {
            return new Class( Names.Repository( Name ) )
                 .WithNamespace( Folder.Infrastructure( Service ), Folder.Data, Folder.Repository )
                 .WithUsings( Using.Harpy_Repository_Entity, Using.IRepositories( Service ), Using.Context( Service ), Using.AgreggateModels( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( RepositoryBase )
                 .WithConstructor( constructor => constructor
                     .WithModifiers( Modifiers.Public )
                     .WithParameter( "context", Names.Context( Service ) )
                     .WithBase( "context" ) )
                 ;
        }
    }
}
