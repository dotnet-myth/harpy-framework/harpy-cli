﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.ComponentModel.DataAnnotations;
using Testura.Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.GenerateCommands.Repository
{

    public class InterfaceCommand : ServiceCommandBase {

        public InterfaceCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-t|--type", Description = "Repository type" )]
        public RepositoryCommand.Type RepositoryType { get; set; } = RepositoryCommand.Type.ReadWrite;

        protected override string FileName => Names.IRepository( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.IRepositories };

        private string RepositoryBase {
            get {
                var repositoryType = Enum.GetName( typeof( RepositoryCommand.Type ), RepositoryType );
                return $"I{ repositoryType }RepositoryAsync<{Names.Model( Name )}>";
            }
        }

        public override Interface GenerateCode( ) {
            return new Interface( Names.IRepository( Name ) )
                 .WithNamespace( Folder.Service( Service ), Folder.IRepositories )
                 .WithUsings( Using.Harpy_Domain_Interfaces_Repositories_Entity, Using.AgreggateModels( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( RepositoryBase );
        }
    }
}
