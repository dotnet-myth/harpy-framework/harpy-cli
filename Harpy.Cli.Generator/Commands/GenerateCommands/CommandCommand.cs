﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "command", "c" )]
    public class CommandCommand : ServiceCommandBase {

        public CommandCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the command" )]
        public string Name { get; set; }

        [Required]
        [Argument( 1, Description = "The type of return" )]
        public string Model { get; set; }

        [Option( Description = "name:type | Property to add in model" )]
        public IEnumerable<NameType> Properties { get; set; }

        protected override string FileName => Names.Command( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.Commands };

        public override Class GenerateCode( ) {
            return new Class( Names.Command( Name ) )
                 .WithNamespace( Folder.Service( Service ), Folder.Commands )
                 .WithUsings( Using.System, Using.System_Linq, Using.System_Collections_Generic, Using.Harpy_Domain_Commands, Using.AgreggateModels( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( $"Command<{Names.Model( Model )}>" )
                 .WithProperties( Properties.AsProperties( ) )
                 .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Protected ) )
                 .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameters( Properties.AsParameters( ) )
                    .WithLogic( logic => logic
                        .WithStatments( Properties.AsStatements( ) ) ) )
                 ;
        }
    }
}