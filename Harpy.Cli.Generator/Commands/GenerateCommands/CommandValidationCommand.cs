﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Testura.Code;
using Testura.Code.Generators.Common.Arguments.ArgumentTypes;
using Testura.Code.Statements;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "commandvalidation", "cmdv", "cv" )]
    public class CommandValidationCommand : ServiceCommandBase {

        public CommandValidationCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the command" )]
        public string Command { get; set; }

        [Option( Description = "name:type | Property to add in model", Template = "-p|--property" )]
        public IEnumerable<NameType> Properties { get; set; }

        [Option( Description = "name:type | Query used in the command", Template = "-q|--query" )]
        public IEnumerable<NameType> Queries { get; set; }

        protected override string FileName => Names.CommandValidation( Command );

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.Validations };

        public override Class GenerateCode( ) {
            return new Class( Names.CommandValidation( Command ) )
                 .WithNamespace( Folder.Service( Service ), Folder.Validations )
                 .WithUsings( Using.System, Using.System_Linq, Using.System_Collections_Generic, Using.Harpy_Domain_Validation, Using.FluentValidation, Using.Commands( Service ), Using.IQueries( Service ), Using.Resources( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( $"Validation<{Names.Command( Command )}>" )
                 .WithFields( Queries.AsQueries( ) )
                 .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameters( Queries.AsParameters( Names.Query( ) ) )
                    .WithLogic( logic => logic
                        .WithStatments( Queries.AsStatements( Names.Query( ) ) )
                        .WithStatments( Properties.AsMethodDeclaration( "Rules" ) ) ) )
                 .WithMethods( ValidationMethods( ) )
                 ;
        }

        private Method[ ] ValidationMethods( ) {
            return Properties.Select( val => {
                return new Method( val.Name.ToFirstUpper( ) + "Rules" )
                            .WithModifiers( Modifiers.Private )
                            .WithLogic( logic => logic
                                 .WithStatments( new ExpressionStatement( )
                                     .Invoke( $"RuleFor( x => x.{val.Name.ToFirstUpper( )} ).NotEmpty().WithMessage",
                                         new List<Testura.Code.Generators.Common.Arguments.ArgumentTypes.Argument> { new ValueArgument( "message" ) } ).AsStatement( ) ) );
            } ).ToArray( );
        }
    }
}