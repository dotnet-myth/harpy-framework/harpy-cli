﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;
using Testura.Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.GenerateCommands
{

    [Command( "specification", "spec", "s" )]
    public class SpecificationCommand : ServiceCommandBase {

        public SpecificationCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        protected override string FileName => Names.Specification( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.Specifications };

        public override Class GenerateCode( ) {
            return new Class( Names.Specification( Name ) )
                 .WithNamespace( Folder.Service( Service ), Folder.Specifications )
                 .WithUsings( Using.Myth_Interfaces, Using.AgreggateModels( Service ) )
                 .WithModifiers( Modifiers.Public, Modifiers.Static )
                 .WithMethod( "WithId", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Static )
                     .WithParameter( "spec", $"ISpec<{Names.Model( Name )}>", ParameterModifiers.This )
                     .WithParameter( "id", typeof( long ) )
                     .WithReturnType( $"ISpec<{Names.Model( Name )}>" )
                     .WithLogic( logic => logic
                        .WithReturn( Reference.Chained(
                            Reference.Variable( "spec" ),
                            Reference.Method( "And",
                                Argument.Lambda( "field",
                                    Reference.Member( $"{Names.Model( Name )}Id" ),
                                    ConditionalStatements.Equal,
                                    Reference.Variable( "id" ) ) ) ) ) ) );

            ;
        }
    }
}
