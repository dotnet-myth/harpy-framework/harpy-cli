﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Commands.GenerateCommands.Query;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli.Directories;

namespace Harpy.Cli.Commands.GenerateCommands
{

    [Command( "query", "q" )]
    public class QueryCommand : MultipleCommandBase {

        public QueryCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
            _directoryResolver = directoryResolver;
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        protected override IEnumerable<ServiceCommandBase> Commands => new List<ServiceCommandBase>  {
            new InterfaceCommand( _directoryResolver){ Name = Name, Force = Force },
            new ImplementationCommand( _directoryResolver){ Name = Name, Force = Force },
            new SpecificationCommand(_directoryResolver){ Name = Name, Force = Force }
        };
    }
}
