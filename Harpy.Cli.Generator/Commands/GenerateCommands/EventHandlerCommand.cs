﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "eventhandler", "eventh", "eh" )]
    public class EventHandlerCommand : ServiceCommandBase {

        public EventHandlerCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the event" )]
        public string Name { get; set; }

        [Option( Description = "name:type | Repository used in the command", Template = "-r|--repository" )]
        public IEnumerable<NameType> Repositories { get; set; }

        [Option( Description = "name:type | Query used in the command", Template = "-q|--query" )]
        public IEnumerable<NameType> Queries { get; set; }

        protected override string FileName => Names.EventHandler( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Application( Service ), Folder.EventHandlers };

        public override Class GenerateCode( ) {
            return new Class( Names.EventHandler( Name ) )
                 .WithNamespace( Folder.Application( Service ), Folder.EventHandlers )
                 .WithUsings( Using.System_Thread, Using.System_Threading_Task, Using.Harpy_Application_EventHandlers, Using.Events( Service ), Using.IRepositories( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( $"EventHandler<{Names.Event( Name )}>" )
                 .WithFields( Repositories.AsRepositories( ) )
                 .WithFields( Repositories.AsQueries( ) )
                 .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameters( Repositories.AsParameters( Names.Repository( ) ) )
                    .WithParameters( Queries.AsParameters( Names.Query( ) ) )
                    .WithLogic( logic => logic
                        .WithStatments( Repositories.AsStatements( Names.Repository( ) ) )
                        .WithStatments( Queries.AsStatements( Names.Query( ) ) ) ) )
                 .WithMethod( "Handle", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Override, Modifiers.Async )
                     .WithReturnType( "Task" )
                     .WithParameter( "notification", Names.Event( Name ) )
                     .WithParameter( "cancellationToken", typeof( CancellationToken ) ) );
            ;
        }
    }
}