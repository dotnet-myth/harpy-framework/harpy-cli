﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "context", "ctx" )]
    public class ContextCommand : ServiceCommandBase {

        public ContextCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        protected override string FileName => Names.Context( Service );

        protected override string[ ] PathPieces => new string[ ] { Projects.Context( Service ) };

        public override Builder GenerateCode( ) {
            return new Class( Names.Context( Service ) )
                .WithNamespace( Folder.Infrastructure( Service ), Folder.Data, Folder.Context )
                .WithUsings( Using.Microsoft_EntityFrameworkCore, Using.Myth_Context, Using.Harpy_IoC_Options )
                .WithModifiers( Modifiers.Public )
                .WithInheritance( "BaseContext" )
                .WithConstructor( constructor => constructor
                    .WithParameter( "options", "DbContextOptions" )
                    .WithParameter( "parameters = null", "Parameters" )
                    .WithModifiers( Modifiers.Public )
                    .WithBase( "options" ) );
        }
    }
}