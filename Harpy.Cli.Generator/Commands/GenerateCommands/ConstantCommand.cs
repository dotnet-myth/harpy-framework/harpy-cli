﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Testura.Code;
using Testura.Code.Generators.Common.Arguments.ArgumentTypes;
using Testura.Code.Models;
using Testura.Code.Models.Types;
using Testura.Code.Statements;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "constant", "const" )]
    public class ConstantCommand : ServiceCommandBase {

        public ConstantCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for constant" )]
        public string Name { get; private set; }

        [Required]
        [Argument( 1, "Type for constant" )]
        public string Type { get; private set; }

        [Option( Description = "name:value | Constants to add", Template = "-c|--constant" )]
        public IEnumerable<NameType> Constants { get; private set; }

        protected override string FileName => Name;

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.Constants };

        public override Builder GenerateCode( ) {
            return new Class( Name )
                .WithNamespace( Folder.Service( Service ), Folder.Constants )
                .WithUsings( Using.Myth_ValueObjects )
                .WithModifiers( Modifiers.Public )
                .WithFields( AsConstants( ) )
                .WithInheritance( $"Constant<{Name}, {Type}>" )
                .WithConstructor( constructor => constructor
                    .WithParameter( "name", "string" )
                    .WithParameter( "value", Type )
                    .WithModifiers( Modifiers.Public )
                    .WithBase( "name" )
                    .WithBase( "value" ) );
        }

        private Field[ ] AsConstants( ) {
            return Constants.Select( constant => {
                return new Field(
                    name: constant.Name,
                    type: CustomType.Create( Name ),
                    modifiers: new List<Modifiers> { Modifiers.Public, Modifiers.Static, Modifiers.Readonly },
                    initializeWith: Statement.Expression.Invoke(
                        $"new {Name}",
                        new List<IArgument> { new VariableArgument( $"nameof({constant.Name})" ), new VariableArgument( constant.TypeName ) } ).AsExpression( ) );
            } ).ToArray( );
        }
    }
}