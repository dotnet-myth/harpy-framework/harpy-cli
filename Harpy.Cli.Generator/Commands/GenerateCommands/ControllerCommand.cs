﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "controller", "ctrl" )]
    public class ControllerCommand : ServiceCommandBase {

        public ControllerCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for controller" )]
        public string Name { get; private set; }

        [Option( Description = "name:type | Property to add in model", Template = "-q|--query" )]
        public IEnumerable<NameType> Queries { get; private set; }

        [Option( Description = "Use the complete methods generation", Template = "-c|--use-complete" )]
        public bool UseCompleteForm { get; set; }

        protected override string FileName => Names.Controller( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Presentation( Service ), Folder.Controllers };

        public override Class GenerateCode( ) {
            if ( UseCompleteForm )
                return GenerateCompleteForm( );
            return GenerateSimpleForm( );
        }

        public Class GenerateCompleteForm( ) {
            return new Class( Names.Controller( Name ) )
                     .WithNamespace( Folder.Presentation( Service ), Folder.Controllers )
                     .WithUsings( Using.AutoMapper, Using.AgreggateModels( Service ), Using.Commands( Service ), Using.IQueries( Service ), Using.Jobs( Service ), Using.Harpy_Domain_Interfaces_Messages, Using.Harpy_Domain_Interfaces_Queries, Using.Harpy_IoC_Extensions, Using.Myth_Api, Using.Myth_Interfaces_Repositories_Results, Using.Harpy_Controller, Using.Harpy_Transaction, Using.Microsoft_AspNetCore_Authorization, Using.Microsoft_AspNetCore_Http, Using.Microsoft_AspNetCore_Mvc, Using.Myth_Api, Using.Harpy_Errors, Using.Nswag_Annotations, Using.ViewModels( Service ), Using.System_Data_Common, Using.System_Thread, Using.System_Threading_Task )
                     .WithModifiers( Modifiers.Public )
                     .WithInheritance( "ApiController" )
                     .WithAttribute( "Route", Argument.Value( $"api/{Service}" ) )
                     .WithAttribute( "OpenApiTags", Argument.Value( Service ) )
                     .WithFields( Queries.AsQueries( ) )
                     .WithConstructor( constructor => constructor
                         .WithModifiers( Modifiers.Public )
                         .WithParameter( "bus", "IMessageBus" )
                         .WithParameter( "mapper", "IMapper" )
                         .WithParameters( Queries.AsParameters( sufix: Names.Query( ) ) )
                         .WithBase( "bus" )
                         .WithBase( "mapper" )
                         .WithLogic( logic => logic.WithStatments( Queries.AsStatements( Names.Query( ) ) ) ) )
                     .WithMethod( "GetAsync", method => method
                         .WithModifiers( Modifiers.Public, Modifiers.Async )
                         .WithAttribute( "OpenApiOperation", Argument.Value( "" ), Argument.Value( "" ) )
                         .WithAttribute( "ProducesResponseType", Argument.Variable( $"typeof( {Name}ViewModel )" ), Argument.Variable( "StatusCodes.Status200OK" ) )
                         .WithAttribute( "HttpGet", Argument.Value( "[controller]" ) )
                         .WithReturnType( "Task<IActionResult>" )
                         .WithParameter( "cancellationToken", typeof( CancellationToken ) )
                         .WithLogic( logic => logic
                            .WithVariable( "response", variable => variable
                                .WithDeclarationType( $"{Name}ViewModel" )
                                .AssignAs( Reference.Member( "null" ) ) )
                            .WithReturn( Reference.Method( "Ok", Argument.Variable( "response" ) ) ) ) )
                     .WithMethod( "PostAsync", method => method
                         .WithModifiers( Modifiers.Public, Modifiers.Async )
                         .WithAttribute( "OpenApiOperation", Argument.Value( "" ), Argument.Value( "" ) )
                         .WithAttribute( "ProducesResponseType", Argument.Variable( $"typeof( {Name}ViewModel )" ), Argument.Variable( "StatusCodes.Status200OK" ) )
                         .WithAttribute( "HttpPost", Argument.Value( "[controller]" ) )
                         .WithReturnType( "Task<IActionResult>" )
                         .WithParameter( $"post{Name}ViewModel", $"Post{Name}ViewModel" )
                         .WithParameter( "cancellationToken", typeof( CancellationToken ) )
                         .WithLogic( logic => logic
                            .WithCustom( "using var transaction = new Transaction" )
                            .WithVariable( "command", variable => variable
                                .WithDeclarationType( )
                                .AssignAs( Reference.Chained(
                                    Reference.Member( "_mapper" ),
                                    Reference.Method( $"Map<Post{Name}Command>", Argument.Variable( $"post{Name}ViewModel" ) ) ) ) )
                            .WithVariable( "result", variable => variable
                                .WithDeclarationType( )
                                .AssignAs( Reference.Chained(
                                    Reference.Member( "await _bus" ),
                                    Reference.Method( $"SendCommandAsync<{Name}>", Argument.Variable( "command" ), Argument.Variable( "cancellationToken" ) ) ) ) )
                            .WithVariable( "response", variable => variable
                                .WithDeclarationType( )
                                .AssignAs( Reference.Chained(
                                    Reference.Member( "_mapper" ),
                                    Reference.Method( $"Map<{Name}ViewModel>", Argument.Variable( "result" ) ) ) ) )
                            .WithCustom( "transaction.Commit" )
                            .WithReturn( Reference.Method( "Created", Argument.Value( "Get" ), Argument.Variable( "response" ) ) ) ) );
        }

        public Class GenerateSimpleForm( ) {
            return new Class( Names.Controller( Name ) )
                         .WithNamespace( Folder.Presentation( Service ), Folder.Controllers )
                         .WithUsings( Using.AutoMapper, Using.AgreggateModels( Service ), Using.Commands( Service ), Using.IQueries( Service ), Using.Jobs( Service ), Using.Harpy_Domain_Interfaces_Messages, Using.Harpy_Domain_Interfaces_Queries, Using.Harpy_IoC_Extensions, Using.Myth_Api, Using.Myth_Interfaces_Repositories_Results, Using.Harpy_Controller, Using.Harpy_Transaction, Using.Microsoft_AspNetCore_Authorization, Using.Microsoft_AspNetCore_Http, Using.Microsoft_AspNetCore_Mvc, Using.Myth_Api, Using.Harpy_Errors, Using.Nswag_Annotations, Using.ViewModels( Service ), Using.System_Data_Common, Using.System_Thread, Using.System_Threading_Task )
                         .WithModifiers( Modifiers.Public )
                         .WithInheritance( "ApiController" )
                         .WithAttribute( "Route", Argument.Value( $"api/{Service}" ) )
                         .WithAttribute( "OpenApiTags", Argument.Value( Service ) )
                         .WithFields( Queries.AsQueries( ) )
                         .WithConstructor( constructor => constructor
                             .WithModifiers( Modifiers.Public )
                             .WithParameter( "bus", "IMessageBus" )
                             .WithParameter( "mapper", "IMapper" )
                             .WithParameters( Queries.AsParameters( sufix: Names.Query( ) ) )
                             .WithBase( "bus" )
                             .WithBase( "mapper" )
                             .WithLogic( logic => logic.WithStatments( Queries.AsStatements( Names.Query( ) ) ) ) )
                         .WithMethod( "GetAsync", method => method
                             .WithModifiers( Modifiers.Public, Modifiers.Async )
                             .WithAttribute( "OpenApiOperation", Argument.Value( "" ), Argument.Value( "" ) )
                             .WithAttribute( "ProducesResponseType", Argument.Variable( $"typeof( {Name}ViewModel )" ), Argument.Variable( "StatusCodes.Status200OK" ) )
                             .WithAttribute( "HttpGet", Argument.Value( "[controller]" ) )
                             .WithReturnType( "Task<IActionResult>" )
                             .WithParameter( "odata", $"Odata<{Name}ViewModel, {Name}>" )
                             .WithParameter( "cancellationToken", typeof( CancellationToken ) )
                             .WithLogic( logic => logic
                                .WithVariable( "response", variable => variable
                                    .WithDeclarationType( $"{Name}ViewModel" )
                                    .AssignAs( Reference.Chained(
                                        Reference.Member( $"await {Name.ToFieldCase( )}Query" ),
                                        Reference.Method( "GetAsync", Argument.Variable( "odata" ), Argument.Variable( "cancellationToken" ) ),
                                        Reference.Method( $"MapToAsync<{Name},{Name}ViewModel>" ) ) ) )
                                .WithReturn( Reference.Method( "Ok", Argument.Variable( "response" ) ) ) ) )
                         .WithMethod( "PostAsync", method => method
                             .WithModifiers( Modifiers.Public, Modifiers.Async )
                             .WithAttribute( "OpenApiOperation", Argument.Value( "" ), Argument.Value( "" ) )
                             .WithAttribute( "ProducesResponseType", Argument.Variable( $"typeof( {Name}ViewModel )" ), Argument.Variable( "StatusCodes.Status200OK" ) )
                             .WithAttribute( "HttpPost", Argument.Value( "[controller]" ) )
                             .WithReturnType( "Task<IActionResult>" )
                             .WithParameter( $"post{Name}ViewModel", $"Post{Name}ViewModel" )
                             .WithParameter( "cancellationToken", typeof( CancellationToken ) )
                             .WithLogic( logic => logic
                                .WithCustom( "using var transaction = new Transaction" )
                                .WithVariable( "response", variable => variable
                                    .WithDeclarationType( $"{Name}ViewModel" )
                                    .AssignAs( Reference.Chained(
                                        Reference.Variable( $"await post{Name}ViewModel" ),
                                        Reference.Method( $"MapTo<Post{Name}Command>" ),
                                        Reference.Method( $"SendAsync", Argument.Variable( "cancellationToken" ) ),
                                        Reference.Method( $"MapToAsync<{Name}, {Name}ViewModel>" ) ) ) )
                                .WithCustom( "transaction.Commit" )
                                .WithReturn( Reference.Method( "Created", Argument.Value( "Get" ), Argument.Variable( "response" ) ) ) ) );
        }
    }
}