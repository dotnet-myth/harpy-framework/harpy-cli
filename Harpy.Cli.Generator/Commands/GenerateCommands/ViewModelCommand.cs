﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Testura.Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.GenerateCommands
{

    [Command( "viewmodel", "vm" )]
    public class ViewModelCommand : ServiceCommandBase {

        public ViewModelCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for view model" )]
        public string Name { get; private set; }

        [Option( Description = "name:type | Property to add in view model", Template = "-p|--property" )]
        public IEnumerable<NameType> Properties { get; private set; }

        protected override string FileName => Names.ViewModel( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Presentation( Service ), Folder.ViewModels };

        public override Class GenerateCode( ) {
            return new Class( Names.ViewModel( Name ) )
                 .WithNamespace( Folder.Presentation( Service ), Folder.ViewModels )
                 .WithUsings( Using.System, Using.System_Linq, Using.System_Collections_Generic )
                 .WithModifiers( Modifiers.Public )
                 .WithProperties( Properties.AsProperties( false ) )
                 ;
        }
    }
}
