﻿using McMaster.Extensions.CommandLineUtils;
using Harpy.Cli.Commands.GenerateCommands;

namespace Harpy.Cli.Commands.GenerateCommands
{

    [Command( "generate", "g" )]
    [Subcommand(
        typeof( ModelCommand ),
        typeof( MapCommand ),
        typeof( RepositoryCommand ),
        typeof( QueryCommand ),
        typeof( SpecificationCommand ),
        typeof( CommandCommand ),
        typeof( CommandHandlerCommand ),
        typeof( CommandValidationCommand ),
        typeof( EventCommand ),
        typeof( EventHandlerCommand ),
        typeof( JobCommand ),
        typeof( JobHandlerCommand ),
        typeof( ControllerCommand ),
        typeof( ContextCommand ),
        typeof( ProfileCommand ),
        typeof( ViewModelCommand ),
        typeof( ConstantCommand ),
        typeof( IoCCommand ),
        typeof( MigrationCommand )
    )]
    public class GenerateCommand {

        public void OnExecute( CommandLineApplication app ) {
            app.ShowHelp( );
        }
    }
}
