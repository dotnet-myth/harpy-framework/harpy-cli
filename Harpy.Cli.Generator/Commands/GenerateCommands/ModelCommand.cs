﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "model", "m" )]
    public class ModelCommand : ServiceCommandBase {

        public ModelCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        [Option( Description = "name:type | Property to add in model", Template = "-p|--property" )]
        public IEnumerable<NameType> Properties { get; set; }

        [Option( Description = "name:type | Fields to add in model", Template = "-f|--field" )]
        public IEnumerable<NameType> Fields { get; set; }

        protected override string FileName => Names.Model( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.AggregateModels };

        public override Class GenerateCode( ) {
            return new Class( Names.Model( Name ) )
                 .WithNamespace( Folder.Service( Service ), Folder.AggregateModels )
                 .WithUsings( Using.System, Using.System_Linq, Using.System_Collections_Generic )
                 .WithModifiers( Modifiers.Public )
                 .WithProperties( Properties.AsProperties( ) )
                 .WithProperties( Fields.AsReaOnlyList( ) )
                 .WithFields( Fields.AsFields( ) )
                 .WithConstructor( constructor => constructor
                     .WithModifiers( Modifiers.Protected ) )
                 .WithConstructor( constructor => constructor
                     .WithModifiers( Modifiers.Public )
                     .WithParameters( Properties.AsParameters( ) )
                     .WithLogic( logic => logic.WithStatments( Properties.AsStatements( ) ) ) )
                 ;
        }
    }
}