﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Testura.Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.GenerateCommands
{

    [Command( "migration", "mi" )]
    public class MigrationCommand : ProjectCommandBase {

        public MigrationCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name of migration" )]
        public string Name { get; set; }

        [Argument( 1, "Date for migration, template: dd/MM/yyyy" )]
        public string Date { get; set; } = DateTime.Now.ToString( "dd/MM/yyyy" );

        private DateTime MigrationDate {
            get {
                try {
                    return DateTime.Parse( Date, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal );
                } catch ( Exception ) {
                    return DateTime.Now;
                }
            }
        }

        protected override string FileName => Names.MigrationFile( Name, MigrationDate );

        protected override string[ ] PathPieces => new string[ ] { "Presentation", Projects.Migration( Project ) };

        public override Builder GenerateCode( ) {
            return new Class( Names.Migration( Name ) )
                .WithNamespace( Folder.PresentationMigrations( Project ) )
                .WithUsings( Using.FluentMigrator )
                .WithModifiers( Modifiers.Public )
                .WithAttribute( $"Migration( {MigrationDate:yyyyMMddHHmmss} )" )
                .WithInheritance( "Migration" )
                .WithMethod( "Up", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Override ) )
                .WithMethod( "Down", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Override ) );
        }
    }
}
