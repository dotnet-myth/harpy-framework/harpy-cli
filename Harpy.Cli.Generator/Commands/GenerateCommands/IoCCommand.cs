﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using Testura.Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.GenerateCommands
{

    [Command( "injectorcontainer", "ioc" )]
    public class IoCCommand : ServiceCommandBase {

        public IoCCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        protected override string FileName => Names.InjectorContainer;

        protected override string[ ] PathPieces => new string[ ] { Projects.IoC( Service ) };

        public override Builder GenerateCode( ) {
            return new Class( Names.InjectorContainer )
                .WithNamespace( Folder.Infrastructure( Service ), Folder.Data, Folder.Context )
                .WithUsings( Using.Harpy_IoC, Using.Microsoft_Extensions_DependencyInjection )
                .WithModifiers( Modifiers.Public, Modifiers.Static )
                .WithMethod( $"Add{Service}", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Static )
                     .WithReturnType( "IServiceCollection" )
                     .WithParameter( "services", "IServiceCollection", ParameterModifiers.This )
                     .WithLogic( logic => logic
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddRepositories" ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddQueries" ) )
                        .WithReturn( Reference.Variable( "services" ) ) ) );
        }
    }
}
