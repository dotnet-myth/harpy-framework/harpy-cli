﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Commands.GenerateCommands.Repository;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli.Directories;

namespace Harpy.Cli.Commands.GenerateCommands
{

    [Command( "repository", "r" )]
    public class RepositoryCommand : MultipleCommandBase {

        public RepositoryCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
            _directoryResolver = directoryResolver;
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-t|--type", Description = "Repository type" )]
        public Type RepositoryType { get; set; } = Type.ReadWrite;

        public enum Type { ReadWrite, Read, Write }

        protected override IEnumerable<ServiceCommandBase> Commands => new List<ServiceCommandBase>  {
            new InterfaceCommand( _directoryResolver){ Name = Name, RepositoryType = RepositoryType, Force = Force},
            new ImplementationCommand( _directoryResolver){ Name = Name, RepositoryType = RepositoryType, Force = Force}
        };
    }
}
