﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Testura.Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.GenerateCommands.Query
{

    public class InterfaceCommand : ServiceCommandBase {

        public InterfaceCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        protected override string FileName => Names.IQuery( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.IQueries };

        public override Interface GenerateCode( ) {
            return new Interface( Names.IQuery( Name ) )
                 .WithNamespace( Folder.Service( Service ), Folder.IQueries )
                 .WithUsings( Using.System_Collections_Generic, Using.System_Threading_Task, Using.System_Thread, Using.Harpy_Domain_Interfaces_Queries, Using.AgreggateModels( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( "IQuery" )
                 .WithMethod( "ExistsAsync", method => method
                     .WithParameter( "id", typeof( long ) )
                     .WithParameter( "cancellationToken", typeof( CancellationToken ) )
                     .WithReturnType( typeof( Task<bool> ) ) );
        }
    }
}
