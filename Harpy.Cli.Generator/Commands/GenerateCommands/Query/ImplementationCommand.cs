﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands.Query {

    public class ImplementationCommand : ServiceCommandBase {

        public ImplementationCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-t|--type", Description = "Repository type" )]
        public RepositoryCommand.Type RepositoryType { get; set; } = RepositoryCommand.Type.ReadWrite;

        protected override string FileName => Names.Query( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Application( Service ), Folder.Queries };

        public override Class GenerateCode( ) {
            return new Class( Names.Query( Name ) )
                 .WithNamespace( Folder.Application( Service ), Folder.Queries )
                 .WithUsings( Using.System_Collections_Generic, Using.System_Threading_Task, Using.System_Thread, Using.Myth_Specifications, Using.IRepositories( Service ), Using.IQueries( Service ), Using.Specification( Service ), Using.AgreggateModels( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( Names.IQuery( Name ) )
                 .WithField( Names.Repository( Name ).ToFieldCase( ), Names.IRepository( Name ), Modifiers.Private, Modifiers.Readonly )
                 .WithConstructor( constructor => constructor
                     .WithModifiers( Modifiers.Public )
                     .WithParameter( Names.Repository( Name ).ToCamelCase( ), Names.IRepository( Name ) )
                     .WithLogic( logic => logic
                         .WithVariable( Names.Repository( Name ).ToFieldCase( ), variable => variable
                             .AssignAs( Names.Repository( Name ).ToCamelCase( ) ) ) ) )
                 .WithMethod( "ExistsAsync", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Async )
                     .WithParameter( "id", typeof( long ) )
                     .WithParameter( "cancellationToken", typeof( CancellationToken ) )
                     .WithReturnType( typeof( Task<bool> ) )
                     .WithLogic( logic => logic
                        .WithVariable( "spec", variable => variable
                            .WithDeclarationType( )
                            .AssignAs(
                                Reference.Chained(
                                    Reference.Variable( $"SpecBuilder<{Names.Model( Name )}>" ),
                                    Reference.Method( "Create" ),
                                    Reference.Method( "WithId",
                                        Argument.Variable( "id" ) ) ) ) )
                        .WithReturn( Reference.Chained(
                            Reference.Variable( $"await {Names.Repository( Name ).ToFieldCase( )}" ),
                            Reference.Method( "AnyAsync", Argument.Variable( "spec" ), Argument.Variable( "cancellationToken" ) ) ) ) ) );

            ;
        }
    }
}