﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "event", "e" )]
    public class EventCommand : ServiceCommandBase {

        public EventCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the event" )]
        public string Name { get; set; }

        [Option( Description = "name:type | Property to add in model" )]
        public IEnumerable<NameType> Properties { get; set; }

        protected override string FileName => Names.Event( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.Events };

        public override Class GenerateCode( ) {
            return new Class( Names.Event( Name ) )
                 .WithNamespace( Folder.Service( Service ), Folder.Events )
                 .WithUsings( Using.Harpy_Domain_Events_Base )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( "Event" )
                 .WithProperties( Properties.AsProperties( ) )
                 .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameters( Properties.AsParameters( ) )
                    .WithLogic( logic => logic
                        .WithStatments( Properties.AsStatements( ) ) ) )
                 ;
        }
    }
}