﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "job", "j" )]
    public class JobCommand : ServiceCommandBase {

        public JobCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the job" )]
        public string Name { get; set; }

        [Option( Description = "name:type | Property to add in model" )]
        public IEnumerable<NameType> Properties { get; set; }

        [Option( Description = "Type of the job", Template = "-t|--type" )]
        public Type TypeJob { get; set; } = Type.Simple;

        public enum Type { Simple, Continued, Persisted }

        protected override string FileName => Names.Job( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Domain( Service ), Folder.Jobs };

        private string JobBase {
            get {
                var type = TypeJob switch {
                    Type.Continued => "ContinuedJob",
                    Type.Persisted => "PersistedJob",
                    _ => "Job",
                };
                return $"{type}";
            }
        }

        public override Class GenerateCode( ) {
            return TypeJob switch {
                Type.Continued => ContinuedJob( ),
                Type.Persisted => PersistedJob( ),
                _ => Job( ),
            };
        }

        private Class Comum( ) {
            return new Class( Names.Job( Name ) )
                 .WithNamespace( Folder.Service( Service ), Folder.Jobs )
                 .WithUsings( Using.Harpy_Domain_Jobs_Base )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( JobBase )
                 .WithProperties( Properties.AsProperties( ) );
        }

        private Class Job( ) {
            return Comum( )
                .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameters( Properties.AsParameters( ) )
                    .WithLogic( logic => logic
                        .WithStatments( Properties.AsStatements( ) ) ) );
        }

        private Class ContinuedJob( ) {
            return Comum( )
                .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameters( Properties.AsParameters( ) )
                    .WithParameter( "parentJob", typeof( string ) )
                    .WithBase( "parentJob" )
                    .WithLogic( logic => logic
                        .WithStatments( Properties.AsStatements( ) ) ) );
        }

        private Class PersistedJob( ) {
            return Comum( )
                .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameters( Properties.AsParameters( ) )
                    .WithParameter( "jobId", typeof( string ) )
                    .WithParameter( "jobCron", typeof( string ) )
                    .WithBase( "jobId" )
                    .WithBase( "jobCron" )
                    .WithLogic( logic => logic
                        .WithStatments( Properties.AsStatements( ) ) ) );
        }
    }
}