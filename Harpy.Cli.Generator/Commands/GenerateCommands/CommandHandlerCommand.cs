﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "commandhandler", "cmdh", "ch" )]
    public class CommandHandlerCommand : ServiceCommandBase {

        public CommandHandlerCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the command" )]
        public string Name { get; set; }

        [Required]
        [Argument( 1, Description = "The type of return" )]
        public string Model { get; set; }

        [Option( Description = "name:type | Repository used in the command", Template = "-r|--repository" )]
        public IEnumerable<NameType> Repositories { get; set; }

        [Option( Description = "name:type | Query used in the command", Template = "-q|--query" )]
        public IEnumerable<NameType> Queries { get; set; }

        protected override string FileName => Names.CommandHandler( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Application( Service ), Folder.CommandHandlers };

        public override Class GenerateCode( ) {
            return new Class( Names.CommandHandler( Name ) )
                 .WithNamespace( Folder.Application( Service ), Folder.CommandHandlers )
                 .WithUsings( Using.System, Using.System_Linq, Using.System_Collections_Generic, Using.System_Thread, Using.System_Threading_Task, Using.Harpy_Application_CommandHandlers, Using.Harpy_Domain_Interfaces_Messages, Using.Harpy_Domain_Events, Using.Harpy_Domain_Interfaces_Validation, Using.MediatR, Using.IRepositories( Service ), Using.IQueries( Service ), Using.AgreggateModels( Service ), Using.Commands( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( $"CommandHandler<{Names.Command( Name )},{Names.Model( Model )}>" )
                 .WithFields( Repositories.AsRepositories( ) )
                 .WithFields( Queries.AsQueries( ) )
                 .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameters( Repositories.AsParameters( sufix: Names.Repository( ) ) )
                    .WithParameters( Queries.AsParameters( sufix: Names.Query( ) ) )
                    .WithParameter( "bus", "IMessageBus" )
                    .WithBase( "bus" )
                    .WithLogic( logic => logic
                        .WithStatments( Repositories.AsStatements( Names.Repository( ) ) )
                        .WithStatments( Queries.AsStatements( Names.Query( ) ) ) ) )
                 .WithMethod( "Handle", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Override, Modifiers.Async )
                     .WithReturnType( $"Task<{Names.Model( Model )}>" )
                     .WithParameter( "command", Names.Command( Name ) )
                     .WithParameter( "cancellationToken", typeof( CancellationToken ) )
                     .WithLogic( logic => logic
                        .WithReturn( Reference.Method( $"new {Names.Model( Model )}" ) ) ) );
            ;
        }
    }
}