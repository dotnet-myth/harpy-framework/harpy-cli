﻿using Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;
using Testura.Code;

namespace Harpy.Cli.Commands.GenerateCommands {

    [Command( "profile", "p" )]
    public class ProfileCommand : ServiceCommandBase {

        public ProfileCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 1, "Name of the source " )]
        public string ModelName { get; private set; }

        [Required]
        [Argument( 2, "Name of the destiny " )]
        public string ViewModelName { get; private set; }

        [Required]
        [Argument( 3, "Type of profile" )]
        public Type ProfileType { get; private set; }

        [Option( "Name of profile", Template = "-n|--name" )]
        public string Name { get; private set; }

        public enum Type { ToViewModel, ToModel, ToCommand }

        protected override string FileName => Names.Profile( RealName );

        protected override string[ ] PathPieces => new string[ ] { Projects.Presentation( Service ), Folder.Mappings, ProfileFolder };

        private string ProfileFolder => ProfileType == Type.ToViewModel ? Folder.DomainToViewModel : Folder.ViewModelToDomain;

        private string RealName => !string.IsNullOrEmpty( Name ) ? Name : ModelName;

        public override Builder GenerateCode( ) {
            var map = $"CreateMap<{Names.Model( ModelName )}, {Names.ViewModel( ViewModelName ).Description}>";
            if ( ProfileType == Type.ToModel )
                map = $"CreateMap<{Names.ViewModel( ViewModelName ).Description}, {Names.Model( ModelName )}>";
            else if ( ProfileType == Type.ToCommand )
                map = $"CreateMap<{Names.ViewModel( ViewModelName ).Description}, {Names.Command( ModelName )}>";

            return new Class( Names.Profile( RealName ) )
                .WithNamespace( Folder.Presentation( Service ), Folder.Mappings, ProfileFolder )
                .WithUsings( Using.AutoMapper, Using.ViewModels( Service ), Using.AgreggateModels( Service ) )
                .WithModifiers( Modifiers.Public )
                .WithInheritance( "Profile" )
                .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithLogic( logic => logic
                         .WithCustom( map, Argument.Variable( "MemberList.None" ) ) ) );
        }
    }
}