﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Testura.Code;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.GenerateCommands
{

    [Command( "map" )]
    public class MapCommand : ServiceCommandBase {

        public MapCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; set; }

        [Option( Description = "name:type | Property to add in model", Template = "-p|--property" )]
        public IEnumerable<NameType> Properties { get; set; }

        protected override string FileName => Names.Map( Name );

        protected override string[ ] PathPieces => new string[ ] { Projects.Context( Service ), Folder.Mappings };

        public override Class GenerateCode( ) {
            return new Class( Names.Map( Name ) )
                 .WithNamespace( Folder.Infrastructure( Service ), Folder.Data, Folder.Context, Folder.Mappings )
                 .WithUsings( Using.Microsoft_EntityFrameworkCore, Using.Microsoft_EntityFrameworkCore_Builders, Using.AgreggateModels( Service ) )
                 .WithModifiers( Modifiers.Public )
                 .WithInheritance( $"IEntityTypeConfiguration<{Names.Model( Name )}>" )
                 .WithMethod( "Configure", method => method
                    .WithModifiers( Modifiers.Public )
                    .WithParameter( "builder", $"EntityTypeBuilder<{Names.Model( Name )}>" )
                    .WithLogic( logic => logic
                        .WithVariable( "builder", variable => variable.ReferenceTo(
                             Reference.Chained(
                                 Reference.Member( "builder" ),
                                 Reference.Method( "ToTable", Argument.Value( Names.Table( Name ) ) ),
                                 Reference.Method( "HasKey", Argument.Lambda( "column", Reference.Member( Names.Model( Name ) + "Id" ) ) ),
                                 Reference.Method( "HasName", Argument.Value( Names.Table( Name ) + "_id" ) ) ) ) )
                        .WithStatments( Properties.AsMap( ) ) ) )
                ;
        }
    }
}
