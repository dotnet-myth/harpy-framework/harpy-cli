﻿using McMaster.Extensions.CommandLineUtils;
using System;
using System.Reflection;

namespace Harpy.Cli.Commands
{

    [Command( "version", "v" )]
    public class VersionCommand {

        private void PrintName( ) {
            Console.WriteLine( @"                   ___ ___                            " );
            Console.WriteLine( @"                  /   |   \_____ _____________ ___.__." );
            Console.WriteLine( @"                 /    ~    \__  \\_  __ \____ <   |  |" );
            Console.WriteLine( @"                 \    Y    // __ \|  | \/  |_> >___  |" );
            Console.WriteLine( @"                  \___|_  /(____  /__|  |   __// ____|" );
            Console.WriteLine( @"                        \/      \/      |__|   \/     " );
        }

        private void PrintLogo( ) {
            Console.WriteLine( @"  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@@@@@@@@@@@@@@@@@#                 %@@@@@@@@@@@@@@@@@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@@@@@@@@@@@                   @@*        ,@@@@@@@@@@@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@@@@@@@                      &@@@@@@@@@       @@@@@@@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@@@@            *              @@@@@@@@@@@@&@    @@@@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@           @@@@@@@&             @@@@@@@@@@@@@.    #@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@          @@@@@@@@@@*               @@@@@@@@@@@@@.    /@@@@@@@" );
            Console.WriteLine( @"  @@@@@@&        @@@@@@@@@@@(                  @@@@@@@@@@@@@@@    @@@@@@" );
            Console.WriteLine( @"  @@@@@       @@@@@@@@@@@@@  @@@@@@          @@@@@@@@@@@@@@@@@@@@  *@@@@" );
            Console.WriteLine( @"  @@@@      @@@@@@@@@@@@@@@  @@@@@@& @@@@@(@@@@@@@@@@@@@@@@@@@@@@&   @@@" );
            Console.WriteLine( @"  @@@     %@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@, .@@" );
            Console.WriteLine( @"  @@#    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#&@@@@@@ @@@  &  @@@ @@@@@@@ @@" );
            Console.WriteLine( @"  @@   @@@@@@@@@@*@@@@@@@@@@@@@@@@@@@@@@@@      &   @     ,@@  &@@**@@@@" );
            Console.WriteLine( @"  @@  @@@@ &@@@*@.@@/ @@@@ @@@@   ,&@@@@@@*                      @   @@@" );
            Console.WriteLine( @"  @@@@@@        , .              &@@@@@@@@,                           @@" );
            Console.WriteLine( @"  @@@@@                   @@@@@@@@@@@@@@@@                             @" );
            Console.WriteLine( @"  @@@@                   @@@@@@@@@@@@@@@@(@@@                          @" );
            Console.WriteLine( @"  @@@                     @@@@@@@ %@@@@@@,@@@@                        @@" );
            Console.WriteLine( @"  @@@               ,@@@     @@@@    @@@@/@@@@                       (@@" );
            Console.WriteLine( @"  @@@@              @@@@@    @@@ @@  ,@@ @@@@@@                      @@@" );
            Console.WriteLine( @"  @@@@@                  @@.   &@@@@@@(   /@@@@ /#                 #@@@@" );
            Console.WriteLine( @"  @@@@@@@                        @   @@        &.                 @@@@@@" );
            Console.WriteLine( @"  @@@@@@@@                                                      %@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@.                                                 @@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@@@@                                             @@@@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@@@@@@@                                       @@@@@@@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@@@@@@@@@@@.                             #@@@@@@@@@@@@@@@@@@@" );
            Console.WriteLine( @"  @@@@@@@@@@@@@@@@@@@@@@@@@@&                *@@@@@@@@@@@@@@@@@@@@@@@@@@" );

            Console.WriteLine( );
            Console.WriteLine( );
        }

        private void PrintSubTitle( ) {
            Console.WriteLine( @"   ___________                                                __    " );
            Console.WriteLine( @"   \_   _____/___________    _____   ______  _  _____________|  | __" );
            Console.WriteLine( @"    |    __) \_  __ \__  \  /     \_/ __ \ \/ \/ /  _ \_  __ \  |/ /" );
            Console.WriteLine( @"    |     \   |  | \// __ \|  Y Y  \  ___/\     (  <_> )  | \/    < " );
            Console.WriteLine( @"    \___  /   |__|  (____  /__|_|  /\___  >\/\_/ \____/|__|  |__|_ \" );
            Console.WriteLine( @"        \/               \/      \/     \/                        \/" );
        }

        public void OnExecute( ) {
            PrintLogo( );
            PrintName( );
            PrintSubTitle( );
            Console.WriteLine( );
            Console.WriteLine( "Developed by: Paulo Leal - https://gitlab.com/harpy-framework" );
            Console.WriteLine( );
            Console.WriteLine( $"Version: {Assembly.GetExecutingAssembly( ).GetName( ).Version}" );
        }
    }
}
