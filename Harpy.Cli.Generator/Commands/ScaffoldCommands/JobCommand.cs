﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static Harpy.Cli.Commands.GenerateCommands.JobCommand;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.ScaffoldCommands
{

    [Command( "job", "j" )]
    public class JobCommand : MultipleCommandBase {

        public JobCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the job" )]
        public string Name { get; set; }

        [Option( Description = "name:type | Property to add in job" )]
        public IEnumerable<NameType> Properties { get; set; }

        [Option( Description = "Type of the job", Template = "-t|--type" )]
        public Type TypeJob { get; set; } = Type.Simple;

        [Option( Description = "name:type | Repository used in the command", Template = "-r|--repository" )]
        public IEnumerable<NameType> Repositories { get; set; }

        [Option( Description = "name:type | Query used in the command", Template = "-q|--query" )]
        public IEnumerable<NameType> Queries { get; set; }

        protected override IEnumerable<CommandBase> Commands => new List<CommandBase>  {
            new Harpy.Cli.Commands.GenerateCommands.JobCommand( _directoryResolver){ Name = Name, Properties = Properties, TypeJob = TypeJob, Force = Force },
            new JobHandlerCommand( _directoryResolver){ Name = Name, Repositories = Repositories, Queries = Queries, Force = Force }
        };
    }
}
