﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.ScaffoldCommands
{

    [Command( "model", "m" )]
    public class ModelCommand : MultipleCommandBase {

        public ModelCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Name for model" )]
        public string Name { get; private set; }

        [Option( Description = "name:type | Property to add in model", Template = "-p|--property" )]
        public IEnumerable<NameType> Properties { get; private set; }

        [Option( Description = "name:type | Fields to add in model", Template = "-f|--field" )]
        public IEnumerable<NameType> Fields { get; private set; }

        protected override IEnumerable<CommandBase> Commands => new List<CommandBase>  {
            new Harpy.Cli.Commands.GenerateCommands.ModelCommand( _directoryResolver){ Name = Name, Properties = Properties, Fields = Fields, Force = Force },
            new MapCommand( _directoryResolver){ Name = Name, Properties = Properties, Force = Force },
            new RepositoryCommand(_directoryResolver){ Name = Name, Force = Force },
            new QueryCommand(_directoryResolver){ Name = Name, Force = Force }
        };
    }
}
