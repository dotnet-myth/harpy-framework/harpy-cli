﻿using McMaster.Extensions.CommandLineUtils;
using Harpy.Cli.Commands.ScaffoldCommands;

namespace Harpy.Cli.Commands.ScaffoldCommands
{

    [Command( "scaffold", "s" )]
    [Subcommand(
        typeof( ModelCommand ),
        typeof( CommandCommand ),
        typeof( EventCommand ),
        typeof( JobCommand )
    )]
    public class ScaffoldCommand {

        public void OnExecute( CommandLineApplication app ) {
            app.ShowHelp( );
        }
    }
}
