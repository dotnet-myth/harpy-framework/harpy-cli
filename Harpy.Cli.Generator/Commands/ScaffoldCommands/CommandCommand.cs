﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.ScaffoldCommands
{

    [Command( "command", "c" )]
    public class CommandCommand : MultipleCommandBase {

        public CommandCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the command" )]
        public string Name { get; set; }

        [Required]
        [Argument( 1, Description = "The type of return" )]
        public string Model { get; set; }

        [Option( Description = "name:type | Property to add in command" )]
        public IEnumerable<NameType> Properties { get; set; }

        [Option( Description = "name:type | Repository used in the command", Template = "-r|--repository" )]
        public IEnumerable<NameType> Repositories { get; set; }

        [Option( Description = "name:type | Query used in the command", Template = "-q|--query" )]
        public IEnumerable<NameType> Queries { get; set; }

        protected override IEnumerable<CommandBase> Commands => new List<CommandBase>  {
            new Harpy.Cli.Commands.GenerateCommands.CommandCommand( _directoryResolver){ Name = Name, Model = Model, Properties = Properties,Force = Force },
            new CommandHandlerCommand( _directoryResolver){ Name = Name, Model = Model, Repositories = Repositories, Queries = Queries, Force = Force },
            new CommandValidationCommand(_directoryResolver){ Command = Name, Properties = Properties, Queries = Queries, Force = Force }
        };
    }
}
