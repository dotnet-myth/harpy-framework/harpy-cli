﻿using Code;
using McMaster.Extensions.CommandLineUtils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli.Directories;
using Harpy.Cli.ValueObjects;

namespace Harpy.Cli.Commands.ScaffoldCommands
{

    [Command( "event", "e" )]
    public class EventCommand : MultipleCommandBase {

        public EventCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, Description = "Name for the event" )]
        public string Name { get; set; }

        [Option( Description = "name:type | Property to add in event" )]
        public IEnumerable<NameType> Properties { get; set; }

        [Option( Description = "name:type | Repository used in the command", Template = "-r|--repository" )]
        public IEnumerable<NameType> Repositories { get; set; }

        [Option( Description = "name:type | Query used in the command", Template = "-q|--query" )]
        public IEnumerable<NameType> Queries { get; set; }

        protected override IEnumerable<CommandBase> Commands => new List<CommandBase>  {
            new Harpy.Cli.Commands.GenerateCommands.EventCommand( _directoryResolver){ Name = Name, Properties = Properties,Force = Force },
            new EventHandlerCommand( _directoryResolver){ Name = Name, Repositories = Repositories, Queries = Queries, Force = Force }
        };
    }
}
