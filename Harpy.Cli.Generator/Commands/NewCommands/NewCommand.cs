﻿using McMaster.Extensions.CommandLineUtils;
using Harpy.Cli.Commands.NewCommands;

namespace Harpy.Cli.Commands.NewCommands
{

    [Command( "new", "n" )]
    [Subcommand( typeof( ProjectCommand ), typeof( ServiceCommand ) )]
    public class NewCommand {

        public void OnExecute( ) {
        }
    }
}
