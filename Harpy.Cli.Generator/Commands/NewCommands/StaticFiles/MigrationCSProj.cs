﻿using Harpy.Cli.ValueObjects;
using System.IO;

namespace Harpy.Cli.Commands.NewCommands.StaticFiles {

    public class MigrationCSProj {

        public static string Generate( string project, string path ) {
            var projectPath = $"{path}/{project}.Migrations.csproj";
            var csproj = $@"<Project Sdk='Microsoft.NET.Sdk'>

  <PropertyGroup>
    {FrameworkVersion.Net6.ToTargetFramework( )}
    <Platforms>AnyCPU;x64</Platforms>
  </PropertyGroup>

  <ItemGroup>
    {Using.FluentMigrator.ToPackageReference( )}
  </ItemGroup>

</Project>".Replace( "'", "\"" );

            File.WriteAllText( projectPath, csproj );
            return projectPath;
        }
    }
}