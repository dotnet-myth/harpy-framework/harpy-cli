﻿using Harpy.Cli.ValueObjects;
using System.IO;

namespace Harpy.Cli.Commands.NewCommands.StaticFiles {

    public class PresentationCSProj {

        public static string Generate( string project, string path ) {
            var projectPath = path + $"/Presentation.Api.csproj";
            var csproj = $@"<Project Sdk='Microsoft.NET.Sdk.Web'>

  <PropertyGroup>
    {FrameworkVersion.Net6.ToTargetFramework( )}
    <Platforms>AnyCPU;x64</Platforms>
  </PropertyGroup>

  <ItemGroup>
    {Using.FluentMigrator.ToPackageReference( )}
    {Using.FluentMigrator_Runner.ToPackageReference( )}
    {Using.Hangfire.ToPackageReference( )}
    {Using.Microsoft_AspNetCore_Newtonsoft.ToPackageReference( )}
    {Using.Microsoft_EntityFrameworkCore_Tools.ToPackageReferenceWithAssets( )}
  </ItemGroup>

  <ItemGroup>
    {Using.Harpy_IoC.ToPackageReference( )}
    {Using.Harpy_SQLite.ToPackageReference( )}
    {Using.Harpy_Transaction.ToPackageReference( )}
    {Using.Harpy_Presentation.ToPackageReference( )}
  </ItemGroup>

  <ItemGroup>
    <ProjectReference Include='..\{project}.Migrations\{project}.Migrations.csproj' />
  </ItemGroup>

</Project>".Replace( "'", "\"" );

            File.WriteAllText( projectPath, csproj );
            return projectPath;
        }
    }
}