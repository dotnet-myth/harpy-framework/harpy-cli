﻿using Code;
using Harpy.Cli.ValueObjects;
using System.IO;
using Testura.Code;

namespace Harpy.Cli.Commands.NewCommands.StaticFiles {

    public class StartupCode {

        public static string Generate( string projectName, string path ) {
            var fullPath = Path.Combine( path, "Startup" );
            new Class( "Startup" )
                .WithNamespace( "Presentation.Api" )
                .WithUsings( Using.Hangfire, Using.Harpy_IoC, Using.Harpy_SQLite_IoC, Using.Harpy_Presentation_Extensions, Using.Harpy_SQLite_Presentation_Extensions, Using.Microsoft_AspNetCore_Builder, Using.Microsoft_AspNetCore_Hosting, Using.Microsoft_Extensions_Configuration, Using.Microsoft_Extensions_DependencyInjection, Using.Microsoft_Extensions_Hosting, Using.Microsoft_Extensions_Logging, Using.Myth_Extensions, Using.System_IO )
                .WithUsing( "Microsoft" )
                .WithField( "_defaultConnection", typeof( string ), Modifiers.Private, Modifiers.Readonly )
                .WithField( "_hangfireConnection", typeof( string ), Modifiers.Private, Modifiers.Readonly )
                .WithField( "_configuration", "IConfiguration", Modifiers.Private, Modifiers.Readonly )
                .WithModifiers( Modifiers.Public )
                .WithConstructor( constructor => constructor
                    .WithModifiers( Modifiers.Public )
                    .WithParameter( "configuration", "IConfiguration" )
                    .WithLogic( variable => variable
                        .WithVariable( "_configuration", variable => variable.AssignAs( "configuration" ) )
                        .WithVariable( "_defaultConnection", variable => variable.AssignAs( "configuration", "GetConnectionStringFile", Argument.Value( "DefaultConnection" ) ) )
                        .WithVariable( "_hangfireConnection", variable => variable.AssignAs( "configuration", "GetConnectionStringFile", Argument.Value( "HangfireConnection" ) ) ) ) )
                .WithMethod( "ConfigureServices", method => method
                     .WithModifiers( Modifiers.Public )
                     .WithParameter( "services", "IServiceCollection" )
                     .WithLogic( logic => logic
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddSwaggerMiddleware", Argument.Member( "AppVersion", "GetCurrent" ) ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddDefaultCorsPolice" ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddCulture", Argument.Value( "en-US" ) ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddDatabase",
                            Argument.Lambda( "connection",
                                Reference.Method( "UseSqliteConnection",
                                    Argument.Variable( "_defaultConnection" ),
                                    Argument.Lambda( "options", Reference.Method( "AddMigrations", Argument.Value( $"{projectName}.Migrations" ) ) ) ) ),
                            Argument.Lambda( "context", Reference.Method( "AddContext<YourContext>" ) ),
                            Argument.Lambda( "options", Reference.Chained(
                                    Reference.Method( "EnableLog" ),
                                    Reference.Method( "EnableMigrations",
                                        Argument.Lambda( "migrationOptions", Reference.Method( "EnsureAllMigrationsApplied" ) ) ) ) ) ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddHarpy",
                             Argument.Lambda( "settings", Reference.Member( "JobStorage", Reference.Chained(
                                Reference.Variable( "HangfireSQLite" ),
                                Reference.Method( "UseStorage",
                                    Argument.Variable( "_hangfireConnection" ) ) ) ) ) ) )
                        .WithVariable( "services", variable => variable.ReferenceTo( "AddEndpoints" ) ) ) )
                .WithMethod( "Configure", method => method
                     .WithModifiers( Modifiers.Public )
                     .WithParameter( "app", "IApplicationBuilder" )
                     .WithLogic( logic => logic
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseStaticFiles", Argument.Lambda( "file", Reference.Chained( Reference.Method( "IncludePath", Argument.Member( "Directory", "GetCurrentDirectory" ), Argument.Value( "images" ) ) ) ) ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseCors", Argument.Value( "DefaultPolicy" ) ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseHarpySwagger" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseHangfireDashboard" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseExceptionMiddleware" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseAuthorizationMiddleware" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseRouting" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseAuthentication" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseAuthorization" ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseEndpoints", Argument.Lambda( "endpoints", Reference.Method( "MapControllers" ) ) ) )
                        .WithVariable( "app", variable => variable.ReferenceTo( "UseHttpsRedirection" ) ) ) )
                .ToFile( fullPath );
            return fullPath;
        }
    }
}