﻿using System.IO;

namespace Harpy.Cli.Commands.NewCommands.StaticFiles {

    public class AppSettings {

        public static void Generate( string project, string path ) {
            var file = $@"{{
  'Logging': {{
    'LogLevel': {{
                'Default': 'Information',
      'Microsoft': 'Warning',
      'Microsoft.Hosting.Lifetime': 'Information'
    }}
        }},
  'AllowedHosts': '*',
  'ConnectionStrings': {{
    'DefaultConnection': 'Data Source={project}.sqlite;',
    'HangfireConnection': 'Data Source={project}_jobs.sqlite;'
  }}
}}".Replace( "'", "\"" );

            File.WriteAllText( path + "/appsettings.json", file );
            File.WriteAllText( path + "/appsettings.Development.json", file );
            File.WriteAllText( path + "/appsettings.Staging.json", file );
            File.WriteAllText( path + "/appsettings.Production.json", file );
        }
    }
}