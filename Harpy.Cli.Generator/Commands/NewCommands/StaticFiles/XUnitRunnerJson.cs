﻿using System.IO;

namespace Harpy.Cli.Commands.NewCommands.StaticFiles {

    public class XUnitRunnerJson {

        public static string Generate( string path ) {
            var fullPath = Path.Combine( path, "xunit.runner.json" );
            var file = $@"{{
  ""parallelizeTestCollections"": false
}}".Replace( "'", "\"" );

            File.WriteAllText( fullPath, file );
            return fullPath;
        }
    }
}