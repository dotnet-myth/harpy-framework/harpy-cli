﻿using Code;
using Harpy.Cli.ValueObjects;
using System.IO;
using Testura.Code;

namespace Harpy.Cli.Commands.NewCommands.StaticFiles {

    public class ProgramCode {

        public static string Generate( string project, string path ) {
            var fullPath = Path.Combine( path, "Program" );
            new Class( "Program" )
                .WithNamespace( "Presentation.Api" )
                .WithUsings( Using.Harpy_Presentation_Extensions, Using.Microsoft_AspNetCore, Using.Microsoft_AspNetCore_Hosting, Using.Microsoft_Extensions_Hosting, Using.System_IO )
                .WithModifiers( Modifiers.Public )
                .WithMethod( "Main", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Static )
                     .WithParameter( "args", typeof( string[ ] ) )
                     .WithLogic( logic => logic
                         .With( Reference.Chained(
                             Reference.Method( "CreateDefaultBuilder", Argument.Variable( "args" ) ),
                             Reference.Method( "Build" ),
                             Reference.Method( "Run" ) ) ) ) )
                .WithMethod( "CreateDefaultBuilder", logic => logic
                     .WithModifiers( Modifiers.Private, Modifiers.Static )
                         .WithReturnType( "IWebHostBuilder" )
                         .WithParameter( "args", typeof( string[ ] ) )
                         .WithLogic( logic => logic
                             .WithReturn( Reference.Chained(
                                 Reference.Variable( "WebHost" ),
                                 Reference.Method( "\n\t\t\t\tCreateDefaultBuilder" ),
                                 Reference.Method( "\n\t\t\t\tUseUrls", Argument.Value( "http://0.0.0.0:5000" ), Argument.Value( "https://0.0.0.0:5001" ) ),
                                 Reference.Method( "\n\t\t\t\tUseKestrel" ),
                                 Reference.Method( "\n\t\t\t\tUseContentRoot", Argument.Variable( "Directory.GetCurrentDirectory( )" ) ),
                                 Reference.Method( "\n\t\t\t\tUseFullLog" ),
                                 Reference.Method( "\n\t\t\t\tUseStartup<Startup>" ) ) ) ) )
                .ToFile( fullPath );
            return fullPath;
        }
    }
}