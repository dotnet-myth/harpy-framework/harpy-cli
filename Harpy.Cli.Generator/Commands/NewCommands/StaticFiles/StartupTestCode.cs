﻿using Code;
using Harpy.Cli.ValueObjects;
using System.IO;
using Testura.Code;

namespace Harpy.Cli.Commands.NewCommands.StaticFiles {

    internal class StartupTestCode {

        public static string Generate( string service, string path ) {
            var fullPath = Path.Combine( path, "Startup" );
            new Class( "Startup" )
                .WithNamespace( Projects.DomainTest( service ) )
                .WithInheritance( "StartupTestBase" )
                .WithUsings( Using.Harpy_IoC, Using.Harpy_SQLite_IoC, Using.Harpy_Test_Domain, Using.Microsoft_AspNetCore_Hosting, Using.Microsoft_Extensions_Configuration, Using.Microsoft_Extensions_DependencyInjection, Using.Microsoft_Extensions_Hosting, Using.Presentation( ), Using.Context( service ), Using.System_IO )
                .WithModifiers( Modifiers.Public )
                .WithMethod( "ConfigureServices", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Override )
                     .WithParameter( "services", "IServiceCollection" )
                     .WithLogic( logic => logic
                        .WithIf( Argument.Variable( "IsFirstCall(services)" ), Argument.Value( "true" ), ConditionalStatements.Equal, body => body
                            .WithVariable( "services", variable => variable.ReferenceTo( "AddDatabase",
                                Argument.Lambda( "connection", Reference.Method( "UseSqliteConnection" ) ),
                                Argument.Lambda( "context", Reference.Method( "AddContext<YourContext>" ) ) ) )
                            .WithVariable( "services", variable => variable.ReferenceTo( "CreateTestDatabase<YourContext>" ) )
                            .WithVariable( "services", Variable => Variable.ReferenceTo( "AddYourContext" ) )
                            .WithCustom( "base.ConfigureServices", Argument.Variable( "services" ) ) ) ) )
                .WithMethod( "ConfigureHost", method => method
                     .WithModifiers( Modifiers.Public, Modifiers.Override )
                     .WithParameter( "hostBuilder", "IHostBuilder" )
                     .WithLogic( logic => logic
                        .WithVariable( "hostBuilder", variable => variable.ReferenceTo( "ConfigureAppConfiguration", Argument.Lambda( "builder", Reference.Method( "AddConfiguration", Argument.Variable( "CreateConfigurationBuilder().Build()" ) ) ) ) )
                        .WithCustom( "base.ConfigureHost", Argument.Variable( "hostBuilder" ) ) ) )
                .ToFile( fullPath );
            return fullPath;
        }
    }
}