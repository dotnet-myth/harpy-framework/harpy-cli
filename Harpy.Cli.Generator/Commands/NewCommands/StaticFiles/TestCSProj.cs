﻿using Harpy.Cli.ValueObjects;
using System.IO;

namespace Harpy.Cli.Commands.NewCommands.StaticFiles {

    public class TestCSProj {

        public static string Generate( string project, string path ) {
            var projectPath = $"{path}/{project}.Test.Presentation.csproj";
            var csproj = $@"<Project Sdk='Microsoft.NET.Sdk'>

  <PropertyGroup>
    {FrameworkVersion.Net6.ToTargetFramework( )}
    <Platforms>AnyCPU;x64</Platforms>
  </PropertyGroup>

  <ItemGroup>
    {Using.Microsoft_AspNetCore_Mvc_Testing.ToPackageReference( )}
    {Using.Microsoft_Test_Sdk.ToPackageReference( )}
    {Using.Coverlet_Collector.ToPackageReference( )}
    {Using.XUnit.ToPackageReference( )}
    {Using.Xunit_Runner_VisualStudio.ToPackageReferenceWithAssets( )}
  </ItemGroup>

  <ItemGroup>
    {Using.Harpy_Test_Presentation.ToPackageReference( )}
    {Using.Myth_Rest.ToPackageReference( )}
  </ItemGroup>

  <ItemGroup>
    <Content Include='xunit.runner.json' CopyToOutputDirectory='PreserveNewest' />
  </ItemGroup>

  <ItemGroup>
    <Folder Include='Scenarios\' />
  </ItemGroup>

</Project>".Replace( "'", "\"" );

            File.WriteAllText( projectPath, csproj );
            return projectPath;
        }
    }
}