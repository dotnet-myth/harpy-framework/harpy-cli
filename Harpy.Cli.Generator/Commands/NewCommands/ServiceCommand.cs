﻿using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli.Commands.NewCommands.StaticFiles;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System.ComponentModel.DataAnnotations;
using System.IO;
using static Harpy.Cli.ValueObjects.Project;

namespace Harpy.Cli.Commands.NewCommands {

    [Command( "service", "s" )]
    public class ServiceCommand : CommandBase {

        public ServiceCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Service name" )]
        public string Service { get; set; }

        public override int OnExecute( ) {
            var projectName = _directoryResolver.GetProjectName( );

            var directory = _directoryResolver.GetCurrentDirectory( );

            var solution = new Solution( projectName );
            solution.Create( Path.Combine( directory.FullName, $"{projectName}.sln" ) );

            var projectDirectory = directory.FullName;

            var serviceDirectory = directory.CreateSubdirectory( Service );

            Print.From( "service" );

            var resourceProject = new Project( Service, ServiceType.Resource );
            resourceProject.Create( projectDirectory );
            solution.AddProject( resourceProject, resourceProject.GetSolutionSubDirectory( ) );
            Print.Create( resourceProject.CurrentFile.FullName );

            var domainProject = new Project( Service, ServiceType.Domain );
            domainProject.Create( projectDirectory );
            solution.AddProject( domainProject, domainProject.GetSolutionSubDirectory( ) );
            domainProject.AddReference( resourceProject );
            domainProject.AddPackages( Using.Harpy_Domain );
            domainProject.AddFolders( Folder.AggregateModels, Folder.Commands, Folder.Constants, Folder.Events, Folder.Interfaces, Folder.IQueries, Folder.IRepositories, Folder.Jobs, Folder.Specifications, Folder.Validations, Folder.ValueObjects );
            Print.Create( domainProject.CurrentFile.FullName );

            var applicationProject = new Project( Service, ServiceType.Application );
            applicationProject.Create( projectDirectory );
            solution.AddProject( applicationProject, applicationProject.GetSolutionSubDirectory( ) );
            applicationProject.AddReference( domainProject );
            applicationProject.AddPackages( Using.Harpy_Application );
            applicationProject.AddFolders( Folder.CommandHandlers, Folder.EventHandlers, Folder.JobHandlers, Folder.Queries );
            Print.Create( applicationProject.CurrentFile.FullName );

            var presentationProject = new Project( Service, ServiceType.Presentation );
            presentationProject.Create( projectDirectory );
            solution.AddProject( presentationProject, presentationProject.GetSolutionSubDirectory( ) );
            presentationProject.AddReference( domainProject );
            presentationProject.AddPackages( Using.Harpy_Presentation, Using.Nswag_Annotations, Using.AutoMapper, Using.Harpy_Transaction );
            presentationProject.AddFolders( Folder.Controllers, Folder.Mappings, Folder.ViewModels );
            Print.Create( presentationProject.CurrentFile.FullName );

            var contextProject = new Project( Service, ServiceType.Context );
            contextProject.Create( projectDirectory );
            solution.AddProject( contextProject, contextProject.GetSolutionSubDirectory( ) );
            contextProject.AddReference( domainProject );
            contextProject.AddPackages( Using.Harpy_IoC, Using.Harpy_Context, Using.Microsoft_EntityFrameworkCore );
            contextProject.AddFolders( Folder.Mappings );
            Print.Create( contextProject.CurrentFile.FullName );

            var repositoryProject = new Project( Service, ServiceType.Repository );
            repositoryProject.Create( projectDirectory );
            solution.AddProject( repositoryProject, repositoryProject.GetSolutionSubDirectory( ) );
            repositoryProject.AddReference( domainProject, contextProject );
            repositoryProject.AddPackages( Using.Harpy_Repository );
            repositoryProject.AddFolders( );
            Print.Create( repositoryProject.CurrentFile.FullName );

            var antiCorruptionProject = new Project( Service, ServiceType.AntiCorruptionLayer );
            antiCorruptionProject.Create( projectDirectory );
            solution.AddProject( antiCorruptionProject, antiCorruptionProject.GetSolutionSubDirectory( ) );
            antiCorruptionProject.AddReference( domainProject, contextProject );
            antiCorruptionProject.AddPackages( );
            antiCorruptionProject.AddFolders( Folder.Entities, Folder.Mappings, Folder.Repositories );
            Print.Create( antiCorruptionProject.CurrentFile.FullName );

            var iocProject = new Project( Service, ServiceType.IoC );
            iocProject.Create( projectDirectory );
            solution.AddProject( iocProject, iocProject.GetSolutionSubDirectory( ) );
            iocProject.AddReference( domainProject, applicationProject, presentationProject, contextProject, repositoryProject, antiCorruptionProject );
            iocProject.AddPackages( Using.Harpy_IoC, Using.Microsoft_Extensions_DependencyInjection_Abstraction );
            iocProject.AddFolders( );
            Print.Create( iocProject.CurrentFile.FullName );

            var testDomainProject = new Project( Service, ServiceType.DomainTest, ProjectType.Test );
            testDomainProject.Create( projectDirectory );
            var testDomainDirectory = testDomainProject.CurrentFile.Directory.FullName;
            solution.AddProject( testDomainProject, testDomainProject.GetSolutionSubDirectory( ) );
            testDomainProject.AddReference( contextProject, iocProject );
            testDomainProject.AddPackages( Using.Harpy_IoC, Using.Harpy_SQLite, Using.Harpy_Presentation, Using.Harpy_Test_Domain, Using.Microsoft_Test_Sdk, Using.XUnit, Using.XUnit_DependecyInjection, Using.Xunit_Runner_VisualStudio, Using.Coverlet_Collector );
            testDomainProject.AddFolders( Folder.Commands );
            StartupTestCode.Generate( Service, testDomainDirectory );
            Print.Create( testDomainProject.CurrentFile.FullName );

            var apiProject = new Project( _directoryResolver.GetProjectName( ), ServiceType.Presentation );
            apiProject.Create( Path.Combine( projectDirectory, "Presentation", "Presentation.Api", "Presentation.Api.csproj" ) );
            apiProject.AddReference( contextProject, iocProject );

            var testPresentationProject = new Project( Service, ServiceType.PresentationTest );
            testPresentationProject.Create( projectDirectory );
            var testDirectory = testPresentationProject.CurrentFile.Directory.FullName;
            TestCSProj.Generate( Service, testDirectory );
            XUnitRunnerJson.Generate( testDirectory );
            solution.AddProject( testPresentationProject, testPresentationProject.GetSolutionSubDirectory( ) );
            testPresentationProject.AddReference( apiProject );
            Print.Create( testPresentationProject.CurrentFile.FullName );

            _directoryResolver.SetCurrentDirectory( serviceDirectory.FullName );

            new IoCCommand( _directoryResolver ).OnExecute( );
            new ContextCommand( _directoryResolver ).OnExecute( );

            return 1;
        }
    }
}