﻿using Harpy.Cli.Commands.Bases;
using Harpy.Cli.Commands.NewCommands.StaticFiles;
using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using Harpy.Cli.ValueObjects;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using static Harpy.Cli.ValueObjects.Project;

namespace Harpy.Cli.Commands.NewCommands {

    [Command( "project", "p" )]
    public class ProjectCommand : CommandBase {

        public ProjectCommand( IDirectoryResolver directoryResolver ) : base( directoryResolver ) {
        }

        [Required]
        [Argument( 0, "Project name" )]
        public string Name { get; private set; }

        public override int OnExecute( ) {
            var directory = _directoryResolver.GetCurrentDirectory( );
            if ( Force ) {
                var projectRoot = new DirectoryInfo( Path.Combine( directory.FullName, Name ) );
                if ( projectRoot.Exists )
                    projectRoot.Delete( true );
            }

            try {
                var solution = new Solution( Name );
                var solutionDirectory = solution.Create( _directoryResolver.GetCurrentDirectory( ).FullName );
                Print.From( "solution" );
                Print.Create( solution.CurrentFile.FullName );

                var presentationDirectory = solutionDirectory.CreateSubdirectory( "Presentation" );
                var apiDirectory = presentationDirectory.CreateSubdirectory( "Presentation.Api" );

                var gitIgnorePath = GitIgnoreFile.Generate( solutionDirectory.FullName );
                Print.Create( gitIgnorePath );

                var presentationProject = new Project( "Presentation.Api", ServiceType.Presentation );
                presentationProject.Create( PresentationCSProj.Generate( Name, apiDirectory.FullName ) );

                Print.From( "api" );
                var programPath = ProgramCode.Generate( Name, apiDirectory.FullName );
                Print.Create( programPath );

                var startupPath = StartupCode.Generate( Name, apiDirectory.FullName );
                Print.Create( startupPath );

                AppSettings.Generate( Name, apiDirectory.FullName );
                Print.Create( Path.Combine( apiDirectory.FullName, "appsetings.json" ) );
                Print.Create( Path.Combine( apiDirectory.FullName, "appsetings.Development.json" ) );
                Print.Create( Path.Combine( apiDirectory.FullName, "appsetings.Staging.json" ) );
                Print.Create( Path.Combine( apiDirectory.FullName, "appsetings.Production.json" ) );

                presentationProject.AddPackages( Using.Harpy_IoC, Using.Harpy_Presentation, Using.Harpy_SQLite, Using.AutoMapper );
                solution.AddProject( presentationProject );

                var migrationDirectory = presentationDirectory.CreateSubdirectory( $"{Name}.Migrations" );
                var migrationProject = new Project( Name, ServiceType.Migration );
                migrationProject.Create( MigrationCSProj.Generate( Name, migrationDirectory.FullName ) );
                Print.From( "migrations" );
                Print.Create( migrationProject.CurrentFile.FullName );
                solution.AddProject( migrationProject );

                return 1;
            } catch ( Exception ) {
                throw;
            }
        }
    }
}