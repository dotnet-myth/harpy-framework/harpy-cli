﻿using System.Collections.Generic;
using Xunit;

namespace Harpy.Cli.Test.Commands.NewCommands {

    public class ProjectCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public ProjectCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "new", "project","--force",
                "Harpy.Template"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Project_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeSolutionMock( );

            // Act
            var result = _fixture.App.Execute( args );

            // Assert
            Assert.Equal( 1, result );
        }
    }
}