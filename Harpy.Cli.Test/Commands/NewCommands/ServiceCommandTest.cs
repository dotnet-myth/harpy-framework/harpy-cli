﻿using Moq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace Harpy.Cli.Test.Commands.NewCommands {

    public class ServiceCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public ServiceCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                 "new", "service",// "--force",
                "Exemple"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Service_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            var calls = 0;
            _fixture.DirectoryResolverMock
                .Setup( x => x.GetCurrentDirectory( ) )
                .Callback( ( ) => calls++ )
                .Returns( ( ) => {
                    var pathPieces = new string[ ] { Path.GetTempPath( ), "Test", "Harpy.Template" }.ToList( );
                    if ( calls > 1 )
                        pathPieces.Add( "Exemple" );
                    return new DirectoryInfo( Path.Combine( pathPieces.ToArray( ) ) );
                } );

            _fixture.DirectoryResolverMock
               .Setup( x => x.GetProjectName( ) )
               .Returns( "Harpy.Template" );

            _fixture.DirectoryResolverMock
               .Setup( x => x.IsServiceDirectory( It.IsAny<DirectoryInfo>( ) ) )
               .Returns( true );

            _fixture.DirectoryResolverMock
               .Setup( x => x.SetCurrentDirectory( It.IsAny<string>( ) ) )
               .Returns( new DirectoryInfo( Path.Combine( Path.GetTempPath( ), "Test", "Harpy.Template", "Exemple" ) ) );

            // Act
            var result = _fixture.App.Execute( args );

            // Assert
            Assert.Equal( 1, result );
        }
    }
}