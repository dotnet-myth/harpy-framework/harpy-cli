﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class MapCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public MapCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "map", "--force",
                "WeatherForecast",
                "-p", "WeatherForecastId",
                "-p", "Date",
                "-p", "TemperatureC",
                "-p", "TemperatureF",
                "-p", "Summary"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Map_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( MapCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.Map.Trim( ), code.Trim( ), ignoreWhiteSpaceDifferences: true );
        }
    }
}