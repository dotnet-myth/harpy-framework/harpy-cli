﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class ProfileCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public ProfileCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "profile", "--force",
                "WeatherForecast",
                "WeatherForecast",
                "ToCommand"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Profile_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( ProfileCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.Profile, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}