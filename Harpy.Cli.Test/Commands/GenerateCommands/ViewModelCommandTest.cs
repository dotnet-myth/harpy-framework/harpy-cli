﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class ViewModelCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public ViewModelCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "viewmodel", "--force",
                "WeatherForecast",
                "-p", "WeatherForecastId:long",
                "-p", "Date:DateTime",
                "-p", "TemperatureC:int",
                "-p", "TemperatureF:int",
                "-p", "Summary:string"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void View_model_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( ViewModelCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.ViewModel, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}