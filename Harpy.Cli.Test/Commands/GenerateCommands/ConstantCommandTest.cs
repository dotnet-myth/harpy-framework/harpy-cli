﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class ConstantCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public ConstantCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "constant", "--force",
                "FileExtension",
                "string",
                "-c", "MKV:.mkv"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Constant_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( ConstantCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.Constant, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}