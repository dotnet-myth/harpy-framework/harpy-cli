﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class EventHandlerCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public EventHandlerCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "eventhandler", "--force",
                "WeatherForecastCreated",
                "-r", "WeatherForecast",
                "-q", "WeatherForecast"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Command_handler_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( EventHandlerCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.EventHandler, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}