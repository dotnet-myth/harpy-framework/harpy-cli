﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands.Repository;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class RepositoryCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public RepositoryCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "repository", "--force",
                "WeatherForecast",
                "-t", "ReadWrite"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Repository_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( ImplementationCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.RepositoryImplementation, code, ignoreWhiteSpaceDifferences: true );
        }

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Repository_interface_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( InterfaceCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.RepositoryInterface, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}