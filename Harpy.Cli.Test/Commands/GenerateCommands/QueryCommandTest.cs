﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands.Query;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class QueryCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public QueryCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "query", "--force",
                "WeatherForecast"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Query_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( ImplementationCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.QueryImplementation, code, ignoreWhiteSpaceDifferences: true );
        }

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Query_interface_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( InterfaceCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.QueryInterface, code, ignoreWhiteSpaceDifferences: true );
        }

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Specification_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( SpecificationCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.Specification, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}