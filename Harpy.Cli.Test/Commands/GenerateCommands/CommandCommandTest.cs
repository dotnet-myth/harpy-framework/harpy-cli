﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class CommandCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public CommandCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "command", "--force",
                "PostWeatherForecast",
                "WeatherForecast",
                "-p", "Date:DateTime",
                "-p", "TemperatureC:int",
                "-p", "Summary:string"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Command_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( CommandCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.Command, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}