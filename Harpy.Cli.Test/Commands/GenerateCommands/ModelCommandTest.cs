﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class ModelCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public ModelCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "model", "--force",
                "WeatherForecast",
                "-p", "WeatherForecastId:long",
                "-p", "Date:DateTime",
                "-p", "TemperatureC:int",
                "-p", "TemperatureF:int",
                "-p", "Summary:string",
                "-f","exemple:Exemple"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Model_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( ModelCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.Model, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}