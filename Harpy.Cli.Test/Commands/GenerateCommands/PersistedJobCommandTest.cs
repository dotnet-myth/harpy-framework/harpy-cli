﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class PersistedJobCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public PersistedJobCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "job", "--force",
                "WeatherForecast",
                "-t", "Persisted",
                "-p", "WeatherForecastId:long"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Continued_job_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( JobCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.PersistedJob, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}