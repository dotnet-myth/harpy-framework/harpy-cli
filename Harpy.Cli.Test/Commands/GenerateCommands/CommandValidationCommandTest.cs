﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class CommandValidationCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public CommandValidationCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "commandvalidation", "--force",
                "PostWeatherForecast",
                "-p", "Date:DateTime",
                "-p", "TemperatureC:int",
                "-q", "WeatherForecast"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Comand_validation_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( CommandValidationCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.CommandValidation, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}