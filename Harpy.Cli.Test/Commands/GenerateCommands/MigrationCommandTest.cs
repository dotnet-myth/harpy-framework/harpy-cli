﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class MigrationCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public MigrationCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "migration", "--force",
                "AddTableWeatherForecast",
                "01/01/2000 00:00"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Context_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeProjectDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( MigrationCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.Migration, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}