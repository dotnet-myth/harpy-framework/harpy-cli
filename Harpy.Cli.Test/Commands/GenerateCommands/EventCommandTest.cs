﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class EventCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public EventCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "event", "--force",
                "WeatherForecastCreated",
                "-p", "WeatherForecastId:long"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Command_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( EventCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.Event, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}