﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.GenerateCommands {

    public class CommandHandlerCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public CommandHandlerCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "generate", "commandhandler", "--force",
                "PostWeatherForecast",
                "WeatherForecast",
                "-r", "WeatherForecast",
                "-q", "WeatherForecast"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Command_handler_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );
            var code = Generated.LastGenerated[ typeof( CommandHandlerCommand ) ].ToString( );

            // Assert
            Assert.Equal( 1, result );
            Assert.Equal( Results.CommandHandler, code, ignoreWhiteSpaceDifferences: true );
        }
    }
}