﻿using System.Collections.Generic;
using Xunit;
using Harpy.Cli.Commands.GenerateCommands;
using Harpy.Cli;

namespace Harpy.Cli.Test.Commands.ScaffoldsCommands {

    public class EventCommandTest : IClassFixture<StandardFixture> {
        private readonly StandardFixture _fixture;

        public EventCommandTest( StandardFixture fixture ) {
            _fixture = fixture;
        }

        public static IEnumerable<object[ ]> Args => new List<object[ ]> {
            new object[] {
                new string[] {
                "scaffold",  "event", "--force",
                "WeatherForecastCreated",
                "-p", "WeatherForecastId:long",
                "-r", "WeatherForecast",
                "-q", "WeatherForecast"
            } } };

        [Theory]
        [MemberData( nameof( Args ) )]
        public void Event_scaffold_must_be_generated_with_success( string[ ] args ) {
            // Arrange
            _fixture.ArrangeServiceDirectoryMock( );

            // Act
            var result = _fixture.App.Execute( args );

            // Assert
            Assert.Equal( 1, result );
            Assert.True( Generated.IsSetted<EventCommand>( ) );
            Assert.True( Generated.IsSetted<EventHandlerCommand>( ) );
        }
    }
}