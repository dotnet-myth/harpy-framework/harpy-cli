﻿using Harpy.Cli.Directories;
using Harpy.Cli.Extensions;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System.IO;

namespace Harpy.Cli.Test {

    public class StandardFixture {
        public CommandLineApplication App { get; }

        public Mock<IDirectoryResolver> DirectoryResolverMock { get; }

        public StandardFixture( ) {
            DirectoryResolverMock = new Mock<IDirectoryResolver>( );

            var services = new ServiceCollection( )
                .AddSingleton<IDirectoryResolver>( DirectoryResolverMock.Object )
                .BuildServiceProvider( );

            Settings.LoadPackageVersions( );

            App = CommandLineApplicationFactory.Create( services );
        }

        public void ArrangeServiceDirectoryMock( ) {
            DirectoryResolverMock
                .Setup( x => x.GetCurrentDirectory( ) )
                .Returns( new DirectoryInfo( Path.Combine( Path.GetTempPath( ), "Test", "Harpy.Template", "exemple" ) ) );

            DirectoryResolverMock
                .Setup( x => x.IsServiceDirectory( It.IsAny<DirectoryInfo>( ) ) )
                .Returns( true );
        }

        public void ArrangeProjectDirectoryMock( ) {
            DirectoryResolverMock
                .Setup( x => x.GetCurrentDirectory( ) )
                .Returns( new DirectoryInfo( Path.Combine( Path.GetTempPath( ), "Test", "Harpy.Template" ) ) );

            DirectoryResolverMock
                .Setup( x => x.IsProjectDirectory( It.IsAny<DirectoryInfo>( ) ) )
                .Returns( true );
        }

        public void ArrangeSolutionMock( ) {
            DirectoryResolverMock
                .Setup( x => x.GetCurrentDirectory( ) )
                .Returns( new DirectoryInfo( Path.Combine( Path.GetTempPath( ), "Test" ) ) );
        }
    }
}